import React from "react";
import App from './containers/App'
import LoginPage from "./containers/LoginPage";
import RegisterPage from "./containers/RegisterPage";
import DashboardPage from "./containers/DashboardPage";
import ConfirmPage from "./containers/ConfirmPage";
import routes from './constants/routes';
import {HashRouter, Switch, Route, Redirect} from "react-router-dom";

// import Recover from "./components/Recover";
// import NewPassword from "./components/NewPassword";

export default () => (
    <App>
        <HashRouter>
            <Switch>
                <Route path={routes.LOGIN} exact={true} component={LoginPage} />
                <Route path={routes.REGISTER} component={RegisterPage} />
                <Route path={routes.REGISTERCODE} component={RegisterPage} />
                <Route path={routes.CONFIRM} component={ConfirmPage} />
                <Route path={routes.DASHBOARD} component={DashboardPage} />
                <Redirect from="*" to="/" />
            </Switch>
        </HashRouter>
    </App>
);