import React from "react";
import { mediaQuery } from "../config/mediaquery";
import { Link, withRouter } from "react-router-dom";
import { api_production } from "../config/global";
import language from "../config/language";
import routes from "../constants/routes";
import Swal from 'sweetalert2';
import Routes from "../Routes";
import { dirname } from "path";

class Login extends React.Component {

    constructor() {
        super();
        this.state = {
            loading:false,
            email: "",
            password: "",
            valid: {
                email: true,
                password: true
            }
        }
        mediaQuery(this, classes, mediaQueries)

        this.login = this.login.bind(this)
    }

    componentWillMount() {
        const token = window.localStorage.getItem("token");
        if (token) {
            window.location = `#${routes.DASHBOARD}`
        }
    }

    validate(email, password) {
        const input_email = document.querySelector('#email')
        const input_password = document.querySelector('#password')
        const class_error = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid red; color: #f1f1f1; padding: 10px "
        const class_success = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid #ffb000; color: #f1f1f1; padding: 10px "

        if (!email) {
            this.setState({ valid: { ...this.state.valid, email: false } })
            input_email.style = class_error
            return false
        } else if (!password) {
            this.setState({ valid: { email: true, password: false } })
            input_email.style = class_success
            input_password.style = class_error
            return false
        } else {
            this.setState({ valid: { email: true, password: true } })
            input_email.style = class_success
            input_password.style = class_success
            return true
        }
    }

    login(e) {
        const { email, password } = this.state

        e.preventDefault();

        if (this.validate(email, password)) {
            this.setState({loading: true})

            fetch(`${api_production}/wallet/login`, {
                method: "POST",
                body: JSON.stringify({ email, password }),
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then(res => res.json())
                .then(res => {
                    if (res.walletNotFound) {
                        Swal.fire({ type: "error", title: language.wallet_not_found_title, text: language.wallet_not_found });
                    } else if (res.notAuthorized) {
                        Swal.fire({
                            type: "warning", title: language.not_authorized_title, text: language.not_authorized, confirmButtonText: language.btn_not_authorized, showLoaderOnConfirm: true,
                            preConfirm: () => {
                                return fetch(`${api_production}/wallet/emailSend`,
                                    {
                                        method: "POST",
                                        body: JSON.stringify({ email }),
                                        headers: {
                                            "Content-Type": "application/json"
                                        }
                                    }
                                )
                            }
                        });
                    } else if (res.oldUser) {
                        Swal.fire({
                            type: "info", title: language.old_user_title, text: language.old_user, showLoaderOnConfirm: true,
                            preConfirm: () => {
                                window.location = `#${routes.REGISTER}`
                            }
                        });
                    } else {
                        window.localStorage.setItem("token", res.obj.token)
                        window.localStorage.setItem("email", res.obj.email)
                        window.localStorage.setItem("name", res.obj.name)
                        window.localStorage.setItem("address", res.obj.address)
                        
                        window.location = `#${routes.DASHBOARD}`

                    }

                    this.setState({loading: false})

                })
        }
    }

    render() {
        const { classes } = this.state

        return (
            <div>
                <main style={classes.CentralizedBlock}>
                    <header style={classes.HeaderBlock}>
                        <img src={require("../img/icon-wallet.png")}></img>
                        <h1 style={classes.HeaderH1}>iZiWallet</h1>
                        <h2 style={classes.HeaderH2}>{language.access_your_account_now}</h2>
                    </header>
                    <form style={classes.FormLogin} id="login">
                        <ul style={classes.MainFormUl}>
                            <li style={classes.MainFormUlLi}>
                                <label htmlFor="email" style={classes.FormLabel}>
                                    <span>{language.input_email}</span>
                                    <input id="email"
                                        placeholder={language.plc_email}
                                        name="email"
                                        type="text"
                                        style={classes.FormInput}
                                        value={this.state.email}
                                        onChange={(e) => this.setState({ email: e.target.value })} />
                                    <span className="icon-Informacao">
                                        {
                                            !this.state.valid.email &&
                                            <i className="fal fa-info-circle" style={classes.FormIcon}></i>
                                        }
                                    </span>
                                </label>
                            </li>
                            <li style={classes.MainFormUlLi}>
                                <label htmlFor="password" style={classes.FormLabel}>
                                    <span>{language.input_password}</span>
                                    <input id="password"
                                        name="password"
                                        type="password"
                                        placeholder={language.plc_password}
                                        style={classes.FormInput}
                                        value={this.state.password}
                                        onChange={(e) => this.setState({ password: e.target.value })} />
                                    <span className={classes.iconInformation}>
                                        {
                                            !this.state.valid.password &&
                                            <i className="fal fa-info-circle" style={classes.FormIcon}></i>
                                        }
                                    </span>
                                </label>
                            </li>
                        </ul>
                        <div style={classes.FormFooter}>
                            <button type="submit" onClick={this.login} style={classes.FormButton}>
                                {
                                    !this.state.loading ?
                                        <p>{language.btn_enter}</p>
                                        :
                                        <div className="spinner">
                                            <div className="bounce1"></div>
                                            <div className="bounce2"></div>
                                            <div className="bounce3"></div>
                                        </div>
                                }

                            </button>
                        </div>
                    </form>
                    <footer style={classes.MainFooter}>
                        <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-around" }}>
                            <Link to={routes.REGISTER} style={{ textDecorationLine: "none", color: "#ffb000", textAlign: "center" }}>{language.btn_create_account}</Link>
                        </div>
                    </footer>
                </main>
            </div>
        )
    }
}

const mediaQueries = {
    CentralizedBlock: {
        l320_768: {

        },
    }
}

const classes = {
    CentralizedBlock: {
        position: "fixed",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
        width: "300px",
        border: "1px solid #ffb000",
        borderRadius: "5px",
        boxShadow: "0 0 5px #ffb00099"
    },

    HeaderBlock: {
        textAlign: "center",
        color: "#f1f1f1",
        padding: "20px"
    },

    HeaderH1: {
        fontSize: "16px"
    },

    HeaderH2: {
        fontSize: "13px",
        marginBottom: 0
    },

    FormLogin: {
        padding: "0 20px",
        margin: 0
    },

    MainFormUl: {
        listStyle: "none",
        padding: 0,
        margin: 0
    },

    MainFormUlLi: {
        margin: "15px 0"
    },

    FormLabel: {
        position: "relative",
        width: "100%",
        fontSize: "11px",
        color: "#f1f1f1"
    },

    FormInput: {
        boxSizing: "border-box",
        width: "100%",
        background: "transparent",
        border: "none",
        borderBottom: "1px solid #ffb000",
        color: "#f1f1f1",
        padding: "10px"
    },

    FormIcon: {
        position: "absolute",
        bottom: "-1px",
        right: "10px",
        color: "#b82423"
    },

    FormFooter: {
        textAlign: "center"
    },

    FormButton: {
        transition: ".2s linear",
        opacity: ".8",
        padding: "0 30px",
        marginTop: "5px",
        backgroundColor: "#ffb000",
        color: "#f1f1f1",
        borderRadius: "5px",
        fontWeight: "700",
        border: "none",
        cursor: "pointer",
        outline:"none"
    },

    LdsRing: {
        display: "inline-block",
        position: "relative",
        width: "5px",
        height: "5px"
    },

    ButtonLoader: {
        boxSizing: "border-box",
        display: "block",
        position: "absolute",
        width: "15px",
        height: "15px",
        marginLeft: "10px",
        marginTop: "-7px",
        border: "2px solid #fff",
        borderRadius: "50%",
        animation: "lds-ring-data-v-6de63560 1.2s cubic-bezier(.5,0,.5,1) infinite",
        borderColor: "#fff transparent transparent"
    },

    MainFooter: {
        textAlign: "center",
        padding: "20px",
        color: "#f1f1f1",
        fontWeight: "700",
        fontSize: "10px"
    }
}

export default withRouter(Login)