import React from "react";
import { mediaQuery } from "../config/mediaquery";
import { api_production } from "../config/global";
import Invite from "./Invite";
import '../app.global.css';
import { verify } from "crypto";
import language from "../config/language";
import routes from "../constants/routes";
import Timer from "./Timer";

import izisymbol from '../img/icon.png'
import walletwhite from '../img/icon-wallet-white.png'

// import { Link } from "react-router-dom";

class Dashboard extends React.Component {

    constructor() {
        super();

        this.state = {
            popupVisible: false,
            modalExitLog: false,
            AbsoluteMenuVisible: false,
            menuActive: 0,
            isClickActive: true,
            userData: {},
            showMessage: false,
            msgStatus: "",
            inviteSystem: false,
            user:{},
            moneyType: "IZI",
            cotations: {
                usd: 93.67
            },
            deadline: Date.now()
        };

        mediaQuery(this, classes, mediaQueries)
        this.checkInviteSystem = this.checkInviteSystem.bind(this)
        this.copyAddress = this.copyAddress.bind(this)
        this.changeMoneyType = this.changeMoneyType.bind(this)
    }

    componentWillMount(){
        fetch("https://www.apilayer.net/api/live?access_key=47cb697f71750e9cdcbf5fda809665ea")
        .then(res => res.json())
        .then(res => {
            let btc = res.quotes.USDBTC * 93.67
            this.setState({cotations: {...this.state.cotations, btc}})
        })
    }

    checkInviteSystem(){

        const address = window.localStorage.getItem("address");

        fetch(`${api_production}/wallet/searchInvites`,{
            method:"POST",
            body: JSON.stringify({address}),
            headers:{
                "Content-Type":"application/json"
            }
        })
        .then(res => res.json())
        .then(res => {
            console.log(res)
            if(res.user){
                this.setState({inviteSystem:true, user: res.user})
            }
        })
    }

    componentDidMount() {
        const token     = window.localStorage.getItem("token");
        const email     = window.localStorage.getItem("email");
        const username  = window.localStorage.getItem("name");

        fetch(`${api_production}/wallet/email`, {
            method: "POST",
            body: JSON.stringify({ email, token }),
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if(res.SessionNotFound){
                    window.location = `#${routes.LOGIN}`
                    window.localStorage.clear();
                }else{
                    this.setState({ userData: res.obj, username })
                }
            })
    }

    operation() {
        this.setState({
            InsideContentVisible: !this.state.InsideContentVisible
        })
    }

    message(msg) {
        this.setState({ msgStatus: msg, showMessage: true }, () => {
            setTimeout(() => this.setState({ showMessage: false }), 2000)
        })
    }


    copyAddress(value)
    {
        console.log(value)
        // navigator.clipboard.writeText(value)
        let copy = document.getElementById('copy-input')
        copy.focus()
        copy.select()

        document.execCommand('copy');
        this.message(language.tooltip_address_copied)
    }

    changeMoneyType(){
        switch(this.state.moneyType){
            case "IZI":
                if(this.state.cotations.usd){
                    this.setState({moneyType: "USD"})
                }
                break
            case "USD":
                if(this.state.cotations.btc){
                    this.setState({moneyType: "BTC"})
                }else{
                    this.setState({moneyType: "IZI"})
                }
                break
            case "BTC":
                this.setState({moneyType: "IZI"})
                break
        }
    }


    exit() {
        window.location = `#${routes.LOGIN}`
        window.localStorage.clear();
    }

    render() {
        const { classes, menuActive, username, userData: { balance, blockedBalance, address } } = this.state
        return (
            <div>
                <div style={classes.MainDash}>
                    {/* MENU */}
                    <div style={classes.BoxMenu}>
                        <div style={classes.Head}>
                            <div style={classes.FlexBox}>
                                <img src={require("../img/icon-wallet.png")} style={classes.FlexBoxImg}></img>
                                <div style={classes.DisplayColumn}>
                                    <h3 style={classes.HeadH3}>iZiMining</h3>
                                    <h4 style={classes.HeadH4}>{username}</h4>
                                </div>
                            </div>
                            <div style={classes.Amount}>
                                <img src={izisymbol} width="30"/>
                                <h4 style={classes.Izicoins}>100.000000</h4>
                                <div style={classes.Withdraw}>
                                    <img src={walletwhite} width="30"/>
                                </div>     
                            </div>
                            <div style={classes.MobileMenu}
                                onClick={() => this.setState({ AbsoluteMenuVisible: true, menuActive: 0 })}>
                                <i className="far fa-bars"></i>
                            </div>
                        </div>
                        <div style={{ marginTop: "-20px" }}>
                            <ul style={{ ...classes.HeadUl, ...classes.HeadUlMobile }}>
                                <li
                                    onClick={() => this.setState({ menuActive: 0 })}
                                    onMouseEnter={() => this.setState({ btnHover: 0 })}
                                    onMouseLeave={() => this.setState({ btnHover: -1 })}
                                    className={this.state.isClickActive && menuActive === 0 ? "Active" : "HeadBtn"}
                                    style={{ ...classes.HeadBtn, backgroundColor: this.state.btnHover === 0 ? "rgb(255, 176, 0)" : "", }}>{language.menu_mining}</li>
                                {
                                    this.state.inviteSystem &&
                                    <li
                                        onClick={() => this.setState({ menuActive: 1, isClickActive: true })}
                                        onMouseEnter={() => this.setState({ btnHover: 1 })}
                                        onMouseLeave={() => this.setState({ btnHover: -1 })}
                                        className={this.state.isClickActive && menuActive === 1 ? "Active" : "HeadBtn"}
                                        style={{ ...classes.HeadBtn, backgroundColor: this.state.btnHover === 1 ? "rgb(255, 176, 0)" : "", }}>{language.menu_invite}</li>
                                }
                                <li

                                    onClick={() => this.setState({ modalExitLog: true })}
                                    onMouseEnter={() => this.setState({ sairHover: true })}
                                    onMouseLeave={() => this.setState({ sairHover: false })}
                                    className="HeadBtn"
                                    style={{ ...classes.HeadBtn, backgroundColor: this.state.sairHover ? "rgb(255, 176, 0)" : "" }}>{language.menu_exit}</li>
                            </ul>
                        </div>
                    </div>

                    {/* TIMER */}
                    <Timer />

                </div>

                {/* MENU MOBILE */}
                <div style={{ ...classes.AbsoluteMenu, display: this.state.AbsoluteMenuVisible ? "flex" : "none" }}>
                    <ul style={classes.AbsoluteMenuUl}>
                        <li style={classes.AbsoluteMenuLi}
                            onClick={() => this.setState({ AbsoluteMenuVisible: false })}
                        >X</li>
                        <li
                            onClick={() => this.setState({ menuActive: 0 })}
                            style={classes.AbsoluteMenuLi}>{language.mining}</li>
                        <li
                            onClick={() => this.setState({ menuActive: 1 })}
                            style={{ ...classes.AbsoluteMenuLi, backgroundColor: this.state.btnHover ? "rgb(255, 176, 0)" : "", }}>{language.menu_invite}</li>
                        <li
                            onClick={() => this.setState({ modalExitLog: true })}
                            style={classes.AbsoluteMenuLi}>{language.menu_exit}</li>
                    </ul>
                </div>

                {/* MODAL SAIR */}
                <div style={{ ...classes.ModalBackdrop, display: this.state.modalExitLog ? "flex" : "none" }}>
                    <div style={classes.DvStlRft} onClick={(e) => { this.setState({ modalExitLog: false }) }}></div>
                    <div style={classes.Modal}>
                        <div style={classes.ModalHead}>
                            <img src={require("../img/exclamation-circle-regular.svg")} style={classes.IconInform}></img>
                        </div>
                        <div style={classes.ModalHead}>
                            <p style={classes.ModalP}>{language.modal_exit}</p>
                        </div>
                        <div style={classes.ModalFooter}>
                            <button
                                onClick={(e) => this.exit()}
                                onMouseEnter={() => this.setState({ yesHover: true })}
                                onMouseLeave={() => this.setState({ yesHover: false })}
                                style={{ ...classes.BtnConfirm, backgroundColor: this.state.yesHover ? "rgb(255, 74, 104)" : "transparent", color: this.state.yesHover ? "white" : "rgb(255, 74, 104)" }}>{language.btn_exit_yes}</button>
                            <button
                                onClick={(e) => { this.setState({ modalExitLog: false }) }}
                                onMouseEnter={() => this.setState({ noHover: true })}
                                onMouseLeave={() => this.setState({ noHover: false })}
                                style={{ ...classes.BtnCancel, backgroundColor: this.state.noHover ? "rgb(255, 176, 0)" : "transparent", color: this.state.noHover ? "white" : "#ffb000" }}>{language.btn_exit_no}</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mask = (value, type, cotations) => {
    switch(type){
        case "IZI":
            return maskIzi(value)
        case "USD":
            return maskUsd(value, cotations.usd)
        case "BTC":
            return maskBtc(value, cotations.btc)
    }
}

const maskUsd = (value, cotation) => {

    let int, dec;
    let izi = maskIzi(value)
    value = String(Number(parseFloat(izi) * cotation).toFixed(8))

    if(value == 0){
        int = "0"
        dec = "00"
    }else{
        value = value.split(".")
        int = value[0]
        dec = value[1].slice(0,2)
    }

    return int + "." + dec
}

const maskBtc = (value, cotation) => {
    let int, dec;
    let izi = maskIzi(value)
    value = String(Number(parseFloat(izi) * cotation).toFixed(8))

    if(value == 0){
        int = "0"
        dec = "00000000"
    }else{
        value = value.split(".")
        int = value[0]
        dec = value[1].slice(0,8)
    }

    return int + "." + dec
}


const maskIzi = (value) => {

    let int = "0", dec = "00000000";

    if (value) {
        value = String(value).split(".").join("")
        if (value.length > 8) {
            int = value.slice(0, value.length - 8)
            dec = value.slice(value.length - 8, value.length)
        } else {
            int = "0"
            dec = new Array(8 - value.length).fill(0).join("") + value
        }
    }

    return int + "." + dec
}

const mediaQueries = {
    l320_947: {
        Amount: {
           
        },
        Amount: {
            margin: 0
        },
        Head: {
            color: "white",
            padding: 0,
            width: "100%"
        },

        MainDash: {
            display: "block",
            height:"100vh"
        },

        BoxMenu: {
            width: "100%",
            height: "56px",
            boxShadow: "0px -7px 20px rgb(192,192,192)",
            paddingTop: "14px"
        },

        MobileMenu: {
            color: "#fff",
            fontSize: "1.5em",
            borderRadius: "10px",
            display: "block",
            marginLeft: "94%",
            marginTop: "-35px",
            cursor: "pointer"
        },

        HeadUlMobile: {
            display: "none"
        },


        FlexBox: {
            marginLeft: 0,
            float: "left",
            justifyContent: "center"
        },

        FlexBoxImg: {
            width: "40px"
        },

        HeadSpan: {
            marginLeft: "10px"
        },

        HeadH4: {
            marginLeft: "0"
        },

        BoxBalance: {
            border: "none",
            height: "auto",
            minWidth: "90%",
            display: "flex",
            justifyContent: "center",
            margin: "auto",
        },

        Balance: {
            margin: "auto",
            width: "98%"
        },

        ModalContent: {
            width: "70%"
        },

        ModalBackdrop: {
            zIndex:"99999"
        }
    }
}

const classes = {
    Izicoins: {
        margin: "0 3px 0 5px", 
        fontSize: "18px",
        
    },
    Withdraw: {
        width: "50px",
        backgroundColor: "#ffb000",
        display: "flex",
        alignItems: "center",
        borderRadius:" 0 5px 5px 0",
        justifyContent:"center",
        marginLeft: "5px",
        padding: "10px 0px 10px 3px"
    },
    DisplayColumn: {
        display:"table-column",
        marginLeft: "10px",
    },
    Amount: {
        width: "180px",
        margin: "20px auto",
        display:"flex",
        paddingLeft:"10px",
        alignItems:"center",
        height: "auto",
        borderRadius: "10px",
        boxShadow: "0px 0px 0px 0px rgba(255, 176, 0, 0.7), 0px 0px 0px 2px #ffb000, 0px 0px 0px 0px inset, 0px 0px 20px rgba(255, 176, 0, 0.5), 0px 0px 20px rgba(255, 176, 0, 0.5)",
        borderColor: "rgba(255, 176, 0.5)"
    },

    MainDash: {
        backgroundColor: "#222",
        display: "flex",
        height: "100%"
    },

    title: {
        color: "white"
    },

    BoxMenu: {
        width: "262px",
        height: "100vh",
        backgroundColor: "#2b2b2b",
    },

    Head: {
        padding: "30px",
        color: "#f1f1f1", 
    },

    FlexBox: {
        display: "-ms-flexbox",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-around",
        width:"202px",
        margin: "auto"
    },

    HeadH3: {
        fontSize: "18px",
        fontWeight: "300",
        textAlign: "center",
        margin: 0
    },

    HeadSpan: {
        fontSize: "11px",
    },

    HeadH4: {
        fontWeight: "300",
        fontSize: "14px",
        textAlign: "center", 
        margin: "0"
    },

    HeadUl: {
        listStyle: "none",
        padding: 0,
    },

    BoxBalance: {
        // width: "30vw",
        height: "100vh",
        borderRight: "2px solid #ffb000",
        maxWidth: "35%",
        minWidth: "20%",
        width: "35%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },

    Balance: {
        width: "75%",
        margin: "auto",
        marginTop: "12px"
    },

    BalanceLabel: {
        color: "#f1f1f1",
        fontWeight: "300",
        fontSize: "11px"
    },

    InputGroup: {
        display: "flex"
    },

    InputLabel: {
        borderRadius: "5px",
        width: "100%",
        background: "transparent",
        border: "1px solid #ffb000",
        padding: "10px",
        bordeRadius: "5px",
        margin: "15px 0",
        color: "#f1f1f1",
        fontSize: "15px",
        fontWeight: "300",
        outline: "none",
        color: "white"
    },

    Tolltip: {
        position: "relative"
    },

    IconCopy: {
        borderRadius: "0 5px 5px 0",
        backgroundColor: "#ffb000",
        border: "none",
        height: "41px",
        width: "45px",
        marginTop: "15px",
        cursor: "pointer",
        outline: "none"
    },

    Copy: {
        display: "inline-block",
        width: "20px",
    },

    BtnAdd: {
        width: "120px",
        height: "40px",
        borderRadius: "5px",
        marginTop: "13px",
        backgroundColor: "transparent",
        border: "1px solid #ffb000",
        fontWeight: "700",
        cursor: "pointer",
        outline: "none",
        transition: "all .4s"
    },

    MainBottom: {
        paddingBottom: "50px"
    },

    Table: {
        alignItems: "center",
        borderBottom: 0,
        backgroundColor: "#222",
        padding: "5px 10px",
        justifyContent: "space-between",
        color: "#f1f1f1",
        display: "grid",
        boxSizing: "border-box",
        marginLeft: "auto",
        margiRight: "auto",
        grid: "auto/30% 15% 18% 17% 20%",
        gridColumnGap: "auto",
        gridRowGap: "auto"
    },

    TableH1: {
        margin: "11px 0",
        fontSize: ".7em",
        display: "grid",
        grid: "auto/15px auto",
        gridColumnGap: "auto",
        gridRowGap: "auto",
        transition: "all .4s",
        alignItems: "center",
        cursor: "pointer"
    },

    IconArrow: {
        fontSize: "13px",
        fill: "#758696"
    },

    Selected: {
        color: "#ffb000"
    },

    MessageH1: {
        padding: "10px",
        boxShadow: "0 0 5px rgb(192,192,192)",
        backgroundColor: "#2b2b2b",
        color: "rgba(53,53,53,.6)",
        textAlign: "center"
    },

    ModalBackdrop: {
        padding: "10px",
        position: "fixed",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        maxHeight: "100%",
        overflowY: "auto",
        backgroundColor: "rgba(0,0,0,.6)",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },

    DvStlRft: {
        minWidth: "100vw",
        minHeight: "100vh",
        position: "relative",
        zIndex: "-1",
        position: "fixed"
    },

    Modal: {
        background: "#2b2b2b",
        border: "1px solid #ffb000",
        overflowX: "auto",
        display: "flex",
        flexDirection: "column",
        padding: "35px",
        boxSizing: "border-box",
        zIndex: "1",
        maxWidth: "500px",
        width: "calc(100% - 20px)",
        margin: "auto",
    },

    AddTransaction: {
        fontSize: "1.6em",
        color: "#f1f1f1"
    },

    Transacao: {
        color: "#ffb000"
    },

    ObsTransaction: {
        color: "#5d5d5d",
        fontSize: "14px"
    },

    FormLabel: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-start",
        color: "#f1f1f1"
    },

    InputBorder: {
        borderRadius: "5px 5px 5px 5px",
        display: "flex"
    },

    FormInput: {
        width: "100%",
        background: "transparent",
        border: "1px solid #ffb000",
        padding: "10px",
        borderRadius: "5px",
        margin: "15px 0",
        color: "#f1f1f1",
        fontSize: "15px",
        fontWeight: "300",
        outline: "none"
    },

    FormFooter: {
        display: "flex",
        justifyContent: "center",
    },

    BtnTransfer: {
        width: "120px",
        height: "40px",
        borderRadius: "5px",
        marginTop: "10px",
        background: "transparent",
        border: "1px solid #ffb000",
        fontWeight: "700",
        cursor: "pointer"
    },

    FooterP: {
        color: "#ff4a68",
        fontSize: ".6em",
        cursor: "pointer",
        textAlign: "center",
        cursor: "pointer"
    },

    MobileMenu: {
        display: "none"
    },

    AbsoluteMenu: {
        width: "200px",
        animation: ".4s slideInRight",
        backgroundColor: "#212227",
        position: "absolute",
        top: 0,
        color: "#fff",
        right: "-1px",
        zIndex: "10",
        minHeight: "100vh",
        textAlign: "right",
        boxShadow: "8px 0px 15px rgb(192,192,192)",
        transition: "all .4s"
    },

    AbsoluteMenuUl: {
        padding: 0,
        listStyle: "none",
        width: "100%"
    },

    AbsoluteMenuLi: {
        cursor: "pointer",
        color: "white",
        padding: "10px 15px"
    },

    ModalExit: {
        zIndex: "9999",
        top: 0,
        position: "absolute",
        transition: "all .4s"
    },

    BgModal: {
        background: "rgba(0,0,0,.6)",
        width: "100vw",
        height: "100vh",
        zIndex: "1"
    },

    ModalAlign: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        width: "100vw"
    },

    ModalContent: {
        zIndex: "9999",
        border: "1px solid #ffb000",
        boxShadow: "0 0 5px rgb(192,192,192)",
        borderRadius: "10px",
        padding: "15px",
        width: "25vw",
        backgroundColor: "#2b2b2b"
    },

    ModalHead: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },

    IconInform: {
        color: "#ff4a68",
        fontSize: "3em",
        width: "50px",
    },

    ModalP: {
        fontSize: "1.4em",
        color: "#f1f1f1",
        fontWeight: "600",
        textAlign: "center"
    },

    ModalFooter: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-evenly",
        marginTop: "30px"
    },

    BtnConfirm: {
        border: "1px solid rgb(255, 74, 104)",
        borderRadius: "5px",
        color: "#f1f1f1",
        padding: "5px",
        width: "90px",
        fontSize: ".8em",
        fontWeight: "500",
        background: "transparent",
        transition: "all .4s",
        background: "#ffb000",
        cursor: "pointer",
        outline: "none"
    },

    BtnCancel: {
        
        padding: "5px",
        border: "1px solid #ffb000",
        background: "transparent",
        width: "90px",
        fontSize: ".8em",
        fontWeight: "500",
        borderRadius: "5px",
        background: "#ffb000",
        transition: "all .4s",
        cursor: "pointer",
        outline: "none"
    },
    MsgStatusArea: {
        position: "fixed",
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "flex-end",
        justifyContent: "center",
        pointerEvents: "none",
    },

    MsgStatus: {
        color: "white",
        background: "#ffb000",
        display: "flex",
        border: "1px solid #ffb000",
        alignItems: "center",
        justifyContent: "center",
        boxShadow: "0px 0px 10px #ffb000",
        minWidth: 150,
        padding: "0px 50px 0px 50px",
        height: 40,
        borderRadius: '5px',
        marginBottom: '40px'
    }
}

export default Dashboard