import React from "react";
import { api_production } from "../config/global";
import { mediaQuery } from "../config/mediaquery";
import InviteListItem from "./InviteListItem";
// import { Link } from "react-router-dom";

class Invite extends React.Component {

    constructor() {
        super();

        this.state = {
            popupVisible: false,
            modalExit: false,
            AbsoluteMenuVisible: false,
            selectedColumn: "none",
            inviteCode: "",
            invites: [],
            enableBonus: false,
            percentage: 50,
            createdWallets: 0,
        };

        mediaQuery(this, classes, mediaQueries)
        this.addPayed = this.addPayed.bind(this)
        this.addInvitesToBonus = this.addInvitesToBonus.bind(this)
        this.copyAddress = this.copyAddress.bind(this)
    }

    operation() {
        this.setState({
            ArrowTableTransformHead2: !this.state.ArrowTableTransformHead2,
        })
    }

    componentDidMount() {

        const { verificationCode, invite } = this.props.user

        this.setState({
            inviteCode: verificationCode,
            invites: invite
        })
        this.mediaQuery()

        fetch(`${api_production}/wallet/getTotal`)
            .then(res => res.json())
            .then(createdWallets => {
                this.setState({ createdWallets: createdWallets.total })
            })
    }

    addPayed(address) {
        fetch(`${api_production}/wallet/transferPromotional01`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                addressFrom: window.localStorage.getItem("address"),
                addressTo: address,
                emailFrom: window.localStorage.getItem("email")
            })
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    Swal.fire({ type: "success", title: "Enviado com sucesso.", text: "Essa pessoa ficará feliz em receber seu presente." })
                    this.setState({
                        invites: this.state.invites.map(el => {
                            if (el.address === address) {

                                return { ...el, status: 1 }
                            } else {
                                return el
                            }
                        })
                    })
                } else {
                    Swal.fire({ type: "error", title: "Não foi possível enviar.", text: "Tente enviar novamente, ou entre em contato conosco." })
                }
            })
    }

    copyAddress() {
        navigator.clipboard.writeText(this.state.inviteCode)
        this.props.msgStatus("Endereço copiado...")
    }

    addInvitesToBonus() {

        let count = parseInt(this.state.invites.filter(el => el.status === 1).length / 10) * 10

        this.setState({
            invites: this.state.invites.map(el => {
                if (count > 0 && el.status === 1) {
                    count--;
                    return { ...el, status: 2 }
                } else {
                    return el
                }
            })
        })

        const addressTo = window.localStorage.getItem("address");

        fetch(`${api_production}/wallet/receiveAward`, {
            method: "POST",
            body: JSON.stringify({ addressTo }),
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.success) {
                    Swal.fire({ type: "success", title: "Bonus adquirido.", text: "Parabéns! você merece bonus pelo seu esforço." })
                } else {
                    Swal.fire({ type: "error", title: "Não foi possível resgatar seu bonus.", text: "Tente resgatar novamente, ou entre em contato conosco." })
                }
            })
    }

    orderTable(param) {
        switch (param) {
            case "invited":
                if (this.state.directionOrder === "up") {
                    this.setState({
                        table: this.state.table.sort((a, b) => a.invited > b.invited ? 1 : -1),
                        directionOrder: "down",
                        selectedColumn: "invited"
                    }, () => console.log(this.state.table))
                } else {
                    this.setState({
                        table: this.state.table.sort((a, b) => a.invited < b.invited ? 1 : -1),
                        directionOrder: "up",
                        selectedColumn: "invited"
                    }, () => console.log(this.state.table))
                }
                break
        }
    }


    render() {
        const { classes } = this.state
        const enableBonus = this.state.invites.filter(el => el.status === 1).length >= 10

        return (
            <div style={classes.TransactionDiv}>
                <div style={classes.BoxTransaction}>

                    {/* POGRESS BAR */}
                    <div style={classes.OpenWalletsContainer}>
                        <p style={{ ...classes.MobileP2, ...classes.flLeft, color: "white" }}>Total de wallets abertas</p>
                        <p style={{ ...classes.MobileP2, ...classes.flRight, color: "white" }}>{`${this.state.createdWallets}/100000`}<i class="fas fa-wallet" style={{ marginLeft: "10px", color: "rgba(255, 255, 255, 1)", fontSize: "20px" }}></i></p>
                        <div style={{ clear: "both" }}></div>
                        <div style={classes.ProgressBar}>
                            <FillProgressBar percentage={this.state.createdWallets} total={100000} />
                        </div>
                    </div>
                    <div style={classes.Transactions}>
                        <div style={classes.Top}>
                            <div style={classes.Tilt}>
                                <h3 style={{ textAlign: "center" }}> Código do Convite </h3>
                            </div>
                            {/* <div style={classes.Search}>
                        <input type="search" placeholder="Procure sua transação" style={classes.SearchTransaction}></input>
                    </div> */}
                            <div style={classes.ContentFlex}>
                                <div style={{ ...classes.MobileP, float: "left" }}>
                                    <p style={classes.InsideP}>{this.state.inviteCode}</p>
                                </div>
                                <button onClick={this.copyAddress} style={classes.IconCopy}><i className="fal fa-copy" style={classes.Copy}></i></button>
                            </div>
                        </div>
                        <div style={classes.Middle}>
                            <div style={classes.Bar}>
                                <div style={{ ...classes.beginBar, backgroundColor: fillBar(0, this.state.invites) ? "#ffb000" : "#222" }}></div>
                                <div style={{ ...classes.barSep, backgroundColor: fillBar(1, this.state.invites) ? "#ffb000" : "#222" }}></div>
                                <div style={{ ...classes.barSep, backgroundColor: fillBar(2, this.state.invites) ? "#ffb000" : "#222" }}></div>
                                <div style={{ ...classes.barSep, backgroundColor: fillBar(3, this.state.invites) ? "#ffb000" : "#222" }}></div>
                                <div style={{ ...classes.barSep, backgroundColor: fillBar(4, this.state.invites) ? "#ffb000" : "#222" }}></div>
                                <div style={{ ...classes.barSep, backgroundColor: fillBar(5, this.state.invites) ? "#ffb000" : "#222" }}></div>
                                <div style={{ ...classes.barSep, backgroundColor: fillBar(6, this.state.invites) ? "#ffb000" : "#222" }}></div>
                                <div style={{ ...classes.barSep, backgroundColor: fillBar(7, this.state.invites) ? "#ffb000" : "#222" }}></div>
                                <div style={{ ...classes.barSep, backgroundColor: fillBar(8, this.state.invites) ? "#ffb000" : "#222" }}></div>
                                <div style={{ ...classes.endBar, backgroundColor: fillBar(9, this.state.invites) ? "#ffb000" : "#222" }}> </div>
                            </div>
                            <div style={classes.BonusBtnContainer}>
                                <div style={{ flex: 1 }}>
                                    <div
                                        onClick={this.addInvitesToBonus}
                                        onMouseEnter={() => this.setState({ addHover: true })}
                                        onMouseLeave={() => this.setState({ addHover: false })}
                                        style={{
                                            ...classes.btnBonus,
                                            backgroundColor: this.state.addHover ? enableBonus ? "#34b75c" : "" : enableBonus ? "" : "",
                                            border: enableBonus ? "1px solid #34b75c" : "1px solid #555",
                                            color: this.state.addHover ? enableBonus ? "white" : "" : enableBonus ? "#34b75c" : "#555",
                                            pointerEvents: enableBonus ? "" : "none"
                                        }}>
                                        <span>Solicitar Bonus</span><span><i class="fas fa-coins"></i>{`  ${parseInt(this.state.invites.filter(el => el.status === 1).length / 10) > 1 ? parseInt(this.state.invites.filter(el => el.status === 1).length / 10) + "x" : ""}`}</span>
                                    </div>
                                </div>
                                <div style={classes.UsersIconContainer}>
                                    <div style={{ color: "#777" }}><i class="fas fa-user" style={{ margin: "0 10px 0 0" }}></i>{this.state.invites.filter(el => el.status === 0).length}</div>
                                    <div style={{ color: "#ffb000" }}><i class="fas fa-user" style={{ margin: "0 10px 0 30px" }}></i>{this.state.invites.filter(el => el.status === 1).length}</div>
                                    <div style={{ color: "#34b75c" }}><i class="fas fa-user-check" style={{ margin: "0 10px 0 30px" }}></i>{this.state.invites.filter(el => el.status === 2).length}</div>
                                </div>
                            </div>
                        </div>
                        <div style={{ ...classes.mainBottom, boxShadow: this.state.invites.length > 0 ? "0 0 4px #C0C0C088" : "" }}>
                            <div style={classes.TableHead}>
                                <article
                                    style={{ ...classes.Article, ...classes.Art1 }}
                                    onClick={() => { this.operation(); this.orderTable("invited") }}>
                                    <h1 style={classes.TableH1}>
                                        <span style={{ ...classes.IconArrow, transform: this.state.ArrowTableTransformHead2 && this.state.selectedColumn == "invited " ? "rotate(180deg)" : "", color: this.state.selectedColumn === "invited" ? "rgb(255, 176, 0)" : "" }}><i className="fas fa-long-arrow-alt-up"></i></span>
                                        <b style={{ ...classes.TextB, color: this.state.selectedColumn === "invited" ? "rgb(255, 176, 0)" : "" }}>Convidado</b>
                                    </h1>
                                </article>
                                <article style={classes.Article} onClick={() => { this.operation(); this.orderTable("date") }}>
                                    <h1 style={classes.TableH1}>
                                        <span style={{ ...classes.IconArrow, transform: this.state.ArrowTableTransformHead2 ? "rotate(180deg)" : "", color: this.state.selectedColumn === "date" ? "rgb(255, 176, 0)" : "" }}><i className="fas fa-long-arrow-alt-up"></i></span>
                                        <b style={{ ...classes.TextB, color: this.state.selectedColumn === "date" ? "rgb(255, 176, 0)" : "" }}>Data</b>
                                    </h1>
                                </article>
                                <article style={classes.Article} onClick={() => { this.operation(); this.orderTable("direction") }}>
                                    <h1 style={classes.TableH1}>
                                        <span style={{ ...classes.IconArrow, transform: this.state.ArrowTableTransformHead2 ? "rotate(180deg)" : "", color: this.state.selectedColumn === "direction" ? "rgb(255, 176, 0)" : "" }}><i className="fas fa-long-arrow-alt-up"></i></span>
                                        <b style={{ ...classes.TextB, color: this.state.selectedColumn === "direction" ? "rgb(255, 176, 0)" : "" }}>Status</b>
                                    </h1>
                                </article>
                            </div>
                            {
                                this.state.invites.length === 0 ?
                                    <div style={classes.Message}>
                                        <h1 style={classes.MessageH1}>
                                            Você não tem convidados pendentes.
                                </h1>
                                    </div>
                                    :
                                    <div style={classes.TableContainer}>
                                        {
                                            this.state.invites.map(item => {
                                                return (
                                                    <InviteListItem data={item} style={this.isTableListVisible} msgStatus={this.props.msgStatus} onPay={(address) => this.addPayed(address)} />
                                                )
                                            })
                                        }
                                    </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const FillProgressBar = (props) => {
    return (
        <div style={{ ...classes.Filler, width: `${props.percentage * 100 / props.total}%` }}></div>
    )
}

const fillBar = (index, value) => {

    value = value.filter(el => el.status === 1).length

    if (value % 10 > index || (value / 10 > 0 && value % 10 === 0)) {
        return true
    } else {
        return false
    }
}

const mediaQueries = {
    l320_768: {
        TransactionDiv: {
            width: "100vw",
            overflow: "initial"
        },
        BoxTransaction: {
            height: "auto",
            overflow: "initial"
        },
        Transactions: {
            height: "auto"
        },

        ItensTable: {
            width: "94%"
        },

        HeadTable: {
            display: "inline-table",
        },

        InsideContent: {
            display: "block"
        },
        MobileP: {
            overflow: "hidden",
        },
        flLeft: {
            float: "none",
            textAlign: "center"
        },
        flRight: {
            float: "none",
            textAlign: "center"
        },
        BonusBtnContainer: {
            display: "block"
        },
        btnBonus: {
            width: "100%"
        },

        UsersIconContainer: {
            justifyContent: "center",
            marginTop: "10px"
        }
    }
}

const classes = {


    TransactionDiv: {
        width: "100vw",
        overflowY: "hidden"
    },

    flLeft: {
        float: "left"
    },

    flRight: {
        float: "right"
    },

    BoxTransaction: {
        width: "100%",
        backgroundColor: "#2b2b2b",
        overflow: "hidden",
        height: "100vh"
    },

    Transactions: {
        width: "90%",
        height: "95%",
        margin: "auto",
        marginTop: "-18px",
        display: "flex",
        flexDirection: "column",
    },

    Top: {
        backgroundColor: "#222",
        borderRadius: 10,
    },

    Middle: {
        paddingBottom: 10
    },

    Bottom: {
        flex: 8,
        height: "60%",
        overflow: "hidden"
    },

    Tilt: {
        color: "#f1f1f1",
        paddingTop: 5
    },

    Search: {
        width: "60%"
    },

    SearchTransaction: {
        width: "80%",
        margin: "auto",
        display: "block",
        border: "none",
        height: "20px",
        padding: "15px 10px",
        borderRadius: "5px",
        marginTop: "16px",
        outline: "none",
        background: "rgb(192,192,192)",
    },

    BtnAdd: {
        width: "120px",
        height: "40px",
        borderRadius: "20px",
        marginTop: "13px",
        backgroundColor: "transparent",
        border: "1px solid #ffb000",
        fontWeight: "700",
        cursor: "pointer",
        outline: "none",
        transition: "all .4s"
    },

    MainBottom: {
        marginBottom: "10px",
        marginTop: "10px",
        display: "flex",
        flexDirection: "column"
    },

    TableHead: {
        alignItems: "center",
        borderBottom: 0,
        backgroundColor: "#222",
        justifyContent: "space-around",
        color: "#f1f1f1",
        display: "flex",
    },

    TableH1: {
        margin: "11px 11px",
        fontSize: ".7em",
        display: "flex",
        justifyContent: "flex-start",
        transition: "all .4s",
        cursor: "pointer",
    },

    IconArrow: {
        display: "flex",
        justifyContent: "center",
        verticalAlign: "middle",
        fontSize: "15px",
        fill: "#758696",
    },

    MessageH1: {
        padding: "10px",
        boxShadow: "0 0 5px rgba(192,192,192, .6)",
        backgroundColor: "#2b2b2b",
        color: "rgba(192,192,192, 0.2)",
        textAlign: "center"
    },

    ModalBackdrop: {
        padding: "10px",
        position: "fixed",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        maxHeight: "100%",
        overflowY: "auto",
        backgroundColor: "rgba(0,0,0,.6)",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },

    DvStlRft: {
        minWidth: "100vw",
        minHeight: "100vh",
        position: "relative",
        zIndex: "-1",
        position: "fixed"
    },

    Modal: {
        background: "#2b2b2b",
        border: "1px solid #ffb000",
        overflowX: "auto",
        display: "flex",
        flexDirection: "column",
        padding: "25px",
        boxSizing: "border-box",
        zIndex: "1",
        maxWidth: "500px",
        width: "calc(100% - 20px)",
        margin: "auto",
    },

    AddTransaction: {
        fontSize: "1.6em",
        color: "#f1f1f1"
    },

    Transacao: {
        color: "#ffb000"
    },

    ObsTransaction: {
        color: "#5d5d5d",
        fontSize: "14px"
    },

    FormLabel: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-start",
        color: "#f1f1f1"
    },

    InputBorder: {
        borderRadius: "5px 5px 5px 5px",
        display: "flex"
    },

    FormInput: {
        width: "100%",
        background: "transparent",
        border: "1px solid #ffb000",
        padding: "10px",
        borderRadius: "5px",
        margin: "15px 0",
        color: "#f1f1f1",
        fontSize: "15px",
        fontWeight: "300",
        outline: "none"
    },

    FormFooter: {
        display: "flex",
    },

    BtnTransfer: {
        width: "120px",
        height: "40px",
        borderRadius: "20px",
        marginTop: "10px",
        background: "transparent",
        border: "1px solid #ffb000",
        fontWeight: "700",
        cursor: "pointer",
        outline: "none",
        marginRight: "20px"
    },

    BtnCancel2: {
        color: "#ff4a68",
        fontSize: ".6em",
        cursor: "pointer",
        textAlign: "center",
        cursor: "pointer",
        width: "120px",
        height: "40px",
        borderRadius: "20px",
        marginTop: "10px",
        background: "transparent",
        border: "1px solid #ff4a68",
        fontWeight: "700",
        cursor: "pointer",
        outline: "none"
    },

    MobileMenu: {
        display: "none"
    },

    AbsoluteMenu: {
        width: "200px",
        animation: ".4s slideInRight",
        backgroundColor: "#212227",
        position: "fixed",
        top: 0,
        color: "#fff",
        right: "-1px",
        zIndex: "10",
        minHeight: "100vh",
        textAlign: "right",
        boxShadow: "-3px 0px 15px rgba(255,255,255, 1)",
        transition: "all .4s"
    },

    AbsoluteMenuUl: {
        padding: 0,
        listStyle: "none",
        width: "100%"
    },

    AbsoluteMenuLi: {
        cursor: "pointer",
        color: "white",
        padding: "10px 15px"
    },

    ModalExit: {
        zIndex: "9999",
        top: 0,
        position: "absolute",
        transition: "all .4s"
    },

    BgModal: {
        background: "rgba(0,0,0,.6)",
        width: "100vw",
        height: "100vh",
        zIndex: "1"
    },

    ModalAlign: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        width: "100vw"
    },

    ModalContent: {
        zIndex: "9999",
        border: "1px solid #ffb000",
        boxShadow: "0 0 5px #ffb000",
        borderRadius: "10px",
        padding: "15px",
        width: "25vw",
        backgroundColor: "#2b2b2b"
    },

    ModalHead: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },

    IconInform: {
        color: "#ff4a68",
        fontSize: "3em"
    },

    ModalP: {
        fontSize: "1.4em",
        color: "#f1f1f1",
        fontWeight: "600",
        textAlign: "center"
    },

    ModalFooter: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-evenly",
        marginTop: "30px"
    },

    BtnConfirm: {
        border: "1px solid #ffb000",
        borderRadius: "20px",
        color: "#f1f1f1",
        padding: "5px",
        width: "90px",
        fontSize: ".8em",
        fontWeight: "500",
        background: "transparent",
        transition: "all .4s",
        cursor: "pointer"
    },

    BtnCancel: {
        border: "1px solid #ffb000",
        padding: "5px",
        width: "90px",
        fontSize: ".8em",
        fontWeight: "500",
        borderRadius: "20px",
        background: "#ffb000",
        transition: "all .4s",
        cursor: "pointer"
    },

    ItensTable: {
        width: "97%",
        justifyContent: "space-between",
        padding: "5px 10px",
        alignItems: "center",
        color: "#758696",
        border: "1px solid rgba(192,192,192, 0.2)",
        transition: "all .4s",
        marginLeft: "auto",
        marginRight: "auto"
    },

    HeadTable: {
        cursor: "pointer",
        display: "grid",
        grid: "auto/30% 15% 18% 17% 20%",
        gridColumnGap: "auto",
        gridRowGap: "auto"
    },

    ItensH2: {
        fontSize: ".75em",
        wordBreak: "break-all",
        color: "#f1f1f1",
        fontWeight: "lighter"
    },

    ValueTransaction: {
        color: "#ffb000",
        fontSize: ".75em",
        wordBreak: "break-all",
        fontWeight: "lighter"
    },

    FlexTransaction: {
        display: "flex"
    },

    Arrow: {
        display: "inline-block",
        verticalAlign: "middle",
        fontSize: "24px",
        color: "#ffb000"
    },
    InsideContent: {
        display: "flex",
        justifyContent: "left",
        cursor: "pointer"
    },

    ContentFlex: {
        display: "flex",
        justifyContent: "space-between",
        flex: "0.30 1 0%",
        padding: "0 20px 10px 20px",
        borderColor: "1px solid #ffb000"
    },

    InsideH3: {
        color: "#f1f1f1",
        fontSize: ".8em"
    },

    InsideP: {
        color: "#f1f1f1",
        fontSize: ".8em",
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
        overflow: "hidden"
    },

    IconCopy: {
        borderRadius: "5px",
        backgroundColor: "#ffb000",
        border: "none",
        height: "32px",
        width: "36px",
        color: "#f1f1f1",
        cursor: "pointer",
        outline: "none",
        float: "right"
    },

    Copy: {
        display: "inline-block",
        fontWeight: "400",
        lineHeight: 1,
        color: "white",
        fontSize: "23px"
    },

    BtnAddTransaction2: {
        display: "flex",
        margin: "auto"
    },

    TableContainer: {
        height: "calc( 100% - 350px )",
        overflow: "scroll",
        borderBottom: "1px solid #C0C0C0"
    },

    Bar: {
        display: "flex",
        width: "100%",
        height: "40px",
        alignItems: "center"
    },

    beginBar: {
        flex: 1,
        borderRadius: "10px 0 0 10px",
        height: "50%",
        backgroundColor: "#63DF19",
        border: "1px solid #222"
    },

    barSep: {
        flex: 1,
        height: "50%",
        backgroundColor: "#63DF19",
        border: "1px solid #222"
    },

    endBar: {
        flex: 1,
        height: "50%",
        borderRadius: "0 10px 10px 0",
        backgroundColor: "#63DF19",
        border: "1px solid #222"
    },

    BonusBtnContainer: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center"
    },

    btnBonus: {
        cursor: "pointer",
        color: "white",
        width: "200px",
        backgroundColor: "#63DF19",
        border: "1px solid white",
        flex: "0 0 60px",
        borderRadius: "10px",
        height: "40px",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-around"
    },

    UsersIconContainer: {
        flex: 1,
        display: "flex",
        justifyContent: "flex-end",
        fontSize: 20
    },

    Article: {
        flex: 1,
        display: "flex",
        justifyContent: "flex-start"
    },

    Art1: {
        flex: 2
    },

    TextB: {
        marginLeft: 5,
    },

    OpenWalletsContainer: {
        width: "83%",
        backgroundColor: "rgb(34, 34, 34)",
        padding: "0 30px 20px 30px",
        margin: "30px auto",
        borderRadius: "10px"

    },

    ProgressBar: {
        position: "relative",
        backgroundColor: "rgba(255, 255, 255, 0.6)",
        height: "20px",
        width: "100%",
        borderRadius: "50px",
        border: "1px solid rgb(34, 34, 34)",
        boxShadow: "0px 0px 5px rgba(255, 255, 255, 0.9)",
        border: "2px solid rgba(255, 255, 255, 0.8)"
    },

    Filler: {
        backgroundColor: "rgba(255, 255, 255, 1)",
        height: "100%",
        borderRadius: "inherit",
        transition: "width .2s ease-in"
    }
}

export default Invite
