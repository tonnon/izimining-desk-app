import React from "react";
import { mediaQuery } from "../config/mediaquery";
import { Link } from "react-router-dom";
import { api_production, client } from "../config/global";

class Recover extends React.Component {

    constructor() {
        super();
        this.state = {
            loading: false,
            name: "",
            email: "",
            valid: {
                email: true,
            },
        }
        mediaQuery(this, classes, mediaQueries)
        this.register = this.register.bind(this)
    }

    componentDidMount() {
        const token = window.localStorage.getItem("token");
        if (token) {
            window.location = client + "/dashboard"
        }
    }

    validate(email) {
        const input_email = document.querySelector('#email')
        const class_error = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid red; color: #f1f1f1; padding: 10px "
        const class_success = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid #ffb000; color: #f1f1f1; padding: 10px "

       if (!email) {
            this.setState({ valid: { email: false } })
            input_email.style = class_error
            return false
        } else {
            this.setState({ valid: { email: true } })
            input_email.style = class_success
            return true   
        }
    }

    register(e) {
        const { email } = this.state;

        e.preventDefault();

        if (this.validate(email)) {
            this.setState({loading: true})

            fetch(`${api_production}/wallet/forgot`, {
                method: "POST",
                body: JSON.stringify({ email }),
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then(res => res.json())
                .then(res => {
                    const input_email = document.querySelector('#email')
                    const class_error = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid red; color: #f1f1f1; padding: 10px "
                    const class_success = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid #ffb000; color: #f1f1f1; padding: 10px "

                    if (res.emailSended) {
                        this.setState({ valid: { email: true } })
                        input_email.style = class_success
                        Swal.fire({ type: "success", title: "Verifique seu email", text: "Verifique o link que enviamos por email para redefinir a sua senha." }).then(() => {
                            window.location = client + "/"
                            this.$router.push("/");
                        });
                    }
                    else if (res.emailFailed) {
                        this.setState({ valid: { email: false } })
                        input_email.style = class_error
                        Swal.fire({ type: "error", title: "Error", text: "Erro ao enviar sua solicitação." })
                    }
                    else if (res.walletNotFound) {
                        this.setState({ valid: { email: false } })
                        input_email.style = class_error
                        Swal.fire({ type: "error", title: "Error", text: "Carteira com este respectivo email não foi encontrado." })
                    }

                    this.setState({loading: false})
                })
        }
    }

    render() {
        const { classes } = this.state

        return (
            <div>
                <main style={classes.CentralizedBlock}>
                    <header style={classes.HeaderBlock}>
                        <img src={require("../img/icon-wallet.png")}></img>
                        <h1 style={classes.HeaderH1}>iZiWallet</h1>
                        <h2 style={classes.HeaderH2}>Recupere a sua senha</h2>
                    </header>
                    <form style={classes.FormLogin} id="login">
                        <ul style={classes.MainFormUl}>
                            <li style={classes.MainFormUlLi}>
                                <label htmlFor="email" style={classes.FormLabel}>
                                    <span>Email</span>
                                    <input id="email"
                                        placeholder="Digite o Email completo"
                                        name="email"
                                        type="text"
                                        style={classes.FormInput}
                                        value={this.state.email}
                                        onChange={(e) => this.setState({ email: e.target.value })} />
                                    <span className="icon-Informacao">
                                        {
                                            !this.state.valid.email &&
                                            <i className="fal fa-info-circle" style={classes.FormIcon}></i>
                                        }
                                    </span>
                                </label>
                            </li>
                        </ul>
                        <div style={classes.FormFooter}>
                            <button type="submit" onClick={this.register} style={classes.FormButton}>
                                {
                                    !this.state.loading ?
                                        <p>Cadastrar</p>
                                        :
                                        <div class="spinner">
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                        </div>
                                }
                            </button>
                        </div>
                    </form>
                    <footer style={classes.MainFooter}>
                        <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-around" }}>
                            <p>
                                Já possui uma conta?
                            <br></br>
                                <Link to="/" style={{ textDecorationLine: "none", color: "#ffb000", textAlign: "center" }}>Login</Link>
                            </p>
                        </div>
                    </footer>
                </main>
            </div>
        )
    }
}


const mediaQueries = {
    CentralizedBlock: {
        l320_768: {

        },
    }
}

const classes = {
    CentralizedBlock: {
        position: "fixed",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
        width: "300px",
        border: "1px solid #ffb000",
        borderRadius: "5px",
        boxShadow: "0 0 5px #ffb00099"
    },

    HeaderBlock: {
        textAlign: "center",
        color: "#f1f1f1",
        padding: "20px"
    },

    HeaderH1: {
        fontSize: "16px"
    },

    HeaderH2: {
        fontSize: "13px",
        marginBottom: 0
    },

    FormLogin: {
        padding: "0 20px",
        margin: 0
    },

    MainFormUl: {
        listStyle: "none",
        padding: 0,
        margin: 0
    },

    MainFormUlLi: {
        margin: "15px 0"
    },

    FormLabel: {
        position: "relative",
        width: "100%",
        fontSize: "11px",
        color: "#f1f1f1"
    },

    FormInput: {
        boxSizing: "border-box",
        width: "100%",
        background: "transparent",
        border: "none",
        borderBottom: "1px solid #ffb000",
        color: "#f1f1f1",
        padding: "10px"
    },

    FormIcon: {
        position: "absolute",
        bottom: "-1px",
        right: "10px",
        color: "#b82423"
    },

    FormFooter: {
        textAlign: "center"
    },

    FormButton: {
        transition: ".2s linear",
        opacity: ".8",
        padding: "0 30px",
        marginTop: "5px",
        backgroundColor: "#ffb000",
        color: "#f1f1f1",
        borderRadius: "20px",
        fontWeight: "700",
        border: "none",
        cursor: "pointer"
    },

    LdsRing: {
        display: "inline-block",
        position: "relative",
        width: "5px",
        height: "5px"
    },

    ButtonLoader: {
        boxSizing: "border-box",
        display: "block",
        position: "absolute",
        width: "15px",
        height: "15px",
        marginLeft: "10px",
        marginTop: "-7px",
        border: "2px solid #fff",
        borderRadius: "50%",
        animation: "lds-ring-data-v-6de63560 1.2s cubic-bezier(.5,0,.5,1) infinite",
        borderColor: "#fff transparent transparent"
    },

    MainFooter: {
        textAlign: "center",
        padding: "20px",
        color: "#f1f1f1",
        fontWeight: "700",
        fontSize: "10px"
    }
}

export default Recover