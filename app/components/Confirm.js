import React from "react";
import { mediaQuery } from "../config/mediaquery";
import { Link } from "react-router-dom";
import { api_production } from "../config/global";

import language from "../config/language";

class Confirm extends React.Component {

    constructor() {
        super();
        mediaQuery(this, classes, mediaQueries)
    }

    componentDidMount() {
        const url_string = window.location.href.split("/#").join("")
        const url = new URL(url_string);

        const email = url.searchParams.get("email");
        const code = url.searchParams.get("validation");
        const token = window.localStorage.getItem("token");

        if (token) {
            window.location = `#${routes.DASHBOARD}`
        } else if (!code || !email) {
            window.location = `#${routes.LOGIN}`
        } else {
            fetch(`${api_production}/wallet/emailConfirmation`, {
                method: "POST",
                body: JSON.stringify({ email, code }),
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then(res => res.json())
                .then(res => {
                    if (res.walletAlreadyExists) {
                        Swal.fire({ type: "info", title: language.wallet_already_exists_title, text: language.wallet_already_exists }).then(() => {
                            
                        });
                    } else if (res.isValid) {
                        Swal.fire({ type: "success", title: language.wallet_created_title, text: language.wallet_created }).then(() => {
                            
                        });
                    }else{
                        Swal.fire({ type: "error", title: language.wallet_created_error_title, text: language.wallet_created_error }).then(() => {
                            
                        });
                    }

                    window.location = `#${routes.LOGIN}`
                })
        }
    }

        render() {
            const { classes } = this.state

            return (
                <div>
                    <main style={classes.CentralizedBlock}>
                        <header style={classes.HeaderBlock}>
                            <img src={require("../img/icon-wallet.png")}></img>
                            <h1 style={classes.HeaderH1}>iZiWallet</h1>
                            <h2 style={classes.HeaderH2}>{language.creating_wallet}</h2>
                        </header>
                        <form style={classes.FormLogin} id="login">
                            <div class="spinner">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                        </form>
                        <footer style={classes.MainFooter}>
                        </footer>
                    </main>
                </div>
            )
        }
    }


const mediaQueries = {
    CentralizedBlock: {
        l320_768: {

        },
    }
}

const classes = {
    CentralizedBlock: {
        position: "fixed",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
        width: "300px",
        border: "1px solid #ffb000",
        borderRadius: "5px",
        boxShadow: "0 0 5px #ffb00099"
    },

    HeaderBlock: {
        textAlign: "center",
        color: "#f1f1f1",
        padding: "20px"
    },

    HeaderH1: {
        fontSize: "16px"
    },

    HeaderH2: {
        fontSize: "13px",
        marginBottom: 0
    },

    FormLogin: {
        padding: "0 20px",
        margin: 0
    },

    MainFormUl: {
        listStyle: "none",
        padding: 0,
        margin: 0
    },

    MainFormUlLi: {
        margin: "15px 0"
    },

    FormLabel: {
        position: "relative",
        width: "100%",
        fontSize: "11px",
        color: "#f1f1f1"
    },

    FormInput: {
        boxSizing: "border-box",
        width: "100%",
        background: "transparent",
        border: "none",
        borderBottom: "1px solid #ffb000",
        color: "#f1f1f1",
        padding: "10px"
    },

    FormIcon: {
        position: "absolute",
        bottom: "-1px",
        right: "10px",
        color: "#b82423"
    },

    FormFooter: {
        textAlign: "center"
    },

    FormButton: {
        transition: ".2s linear",
        opacity: ".8",
        padding: "0 30px",
        marginTop: "5px",
        backgroundColor: "#ffb000",
        color: "#f1f1f1",
        borderRadius: "20px",
        fontWeight: "700",
        border: "none",
        cursor: "pointer",
        outline: "none"
    },

    LdsRing: {
        display: "inline-block",
        position: "relative",
        width: "5px",
        height: "5px"
    },

    ButtonLoader: {
        boxSizing: "border-box",
        display: "block",
        position: "absolute",
        width: "15px",
        height: "15px",
        marginLeft: "10px",
        marginTop: "-7px",
        border: "2px solid #fff",
        borderRadius: "50%",
        animation: "lds-ring-data-v-6de63560 1.2s cubic-bezier(.5,0,.5,1) infinite",
        borderColor: "#fff transparent transparent"
    },

    MainFooter: {
        textAlign: "center",
        padding: "20px",
        color: "#f1f1f1",
        fontWeight: "700",
        fontSize: "10px"
    }
}

export default Confirm