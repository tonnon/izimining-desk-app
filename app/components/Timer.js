import React, { Component } from 'react';

import izisymbol from '../img/icon.png'

class Timer extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      times: [],
      startTime: 0,
      timeElasped: 0,
      stage: 'Iniciar',
      timeInterval: null,
      random: 0,
      coins: 0
    }
  }
  
    
    handleTimerClick() {
      const {stage, timeElasped} = this.state;
      const min = 1;
      const max = 100;
      const rand = min + Math.random() * (max - min);
      if(stage == 'Iniciar') {
         this.setState({
          startTime: new Date(),
          stage: 'Terminar',
        }),
        this.setState({ 
          random: this.state.random + rand 
        });
        this.startTimer();
      } else if(stage == 'Terminar') {
         this.setState({
           stage: 'reset'
         })
  
        clearInterval(this.timeInterval);
        this.addScore(timeElasped);
      } else {
        this.resetTimer();
      }
     
    }
    
    startTimer() {
      this.timeInterval = setInterval(() => this.updateTimer(), 10);
    }
    updateTimer() {
      const {startTime} = this.state;
      const now = new Date();
      const timeElasped = new Date(now - startTime);
      this.setState({timeElasped});
    }
  
   addScore(time) {
      const timeInMilli = time.getUTCHours() * 60 + time.getUTCMinutes() * 60000 + time.getUTCSeconds() * 1000 + time.getUTCMilliseconds();
      const {times} = this.state;
      const newTimes = times.slice();
      newTimes.push(timeInMilli);
      newTimes.sort((b, a) => b - a);
      this.setState({times: newTimes});
    }
  
     resetTimer() {
       this.setState({
         timeElasped: 0,
         startTime: 0,
         stage: 'Iniciar',
         random: 0,
       })
     }
  
  
    convertTimeToDisplay(time) {
      const {stage, timeElasped} = this.state
      if(stage == 'Iniciar' || timeElasped == 0) return '00h : 00min : 00s .00mils';
      
      const {hours, minutes, seconds, milliseconds} = this.getTimeValues(time);
      
      const maybeMinutes = minutes > 0 ? `${minutes}min :` : `` ;
      const maybeHours = hours > 0 ? `${hours}h :` : '';
  
      const timeDisplay = `${hours}h : ${minutes}min : ${seconds}s .${milliseconds}mils`;
      return timeDisplay;
    } 
  
    getTimeValues(time) {
      const randomNumber = this.state.random;
      const hours = padTime(time.getUTCHours());
      const minutes = padTime(time.getUTCMinutes());
      const seconds = padTime(time.getUTCSeconds());
      const milliseconds = padMiliseconds(time.getUTCMilliseconds());
      return {hours, minutes, seconds, milliseconds, randomNumber};
    }
  
  
  
    render() {
      const {times, stage, timeElasped, coins} = this.state;
      return (
       
        <div className="page">

          <div className="timer-container">
              <div>
                <IziMined coins={coins}/>
              <h1>{this.convertTimeToDisplay(timeElasped)}</h1>
            <ButtonTimer handleTimerClick={() => this.handleTimerClick()} stage={stage} />
            </div>
            <TimeContainer times={times}/>
          </div>
         </div>
      )
    }
  }
  
  const ButtonTimer = ({handleTimerClick, stage}) => {
    return (
       <button onClick={() => handleTimerClick()} className={`btn--${stage}`}>{stage}</button>
    )
  }
  
const IziMined = ({coins}) => {
  return (
    <div className="card-block">
    <img src={izisymbol} width="25"/> <h4 style={{margin: 10}} coins={coins}>{iziMinedDisplay(coins)}</h4>
  </div> 
  )
}

  const TimeContainer = ({times, coins}) => {
    return (
      <div className="times-list">
       
        <TimeList times={times} coins={coins} />
      </div>
    )
  }
  
  const TimeList = ({times, coins}) => {
    return (
      <div className="times-list_list">
        {times.map((time, index) => {
          return <TimeScore key={index} time={time} index={index + 1} coins={coins}/>
        })}
      </div>
    )
  }
  
  const TimeScore = ({time, index, coins}) => {
    return (
      <div className={`times-list_time times-list_time--${index}`}>
        <span className="">{index}</span>
        <h4 className="times-list_time_time">{millisecondsToTimeDisplay(time)}</h4>
        <h4 className="times-list_time_time">{iziMinedDisplay(coins)}</h4>
      </div>
    )
  }
  
  // Helpers
  
  function millisecondsToTimeDisplay(time) {
      const hours = padTime((Math.floor % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      const minutes = padTime(Math.floor(time / (1000 * 60) % 60));
      const seconds = padTime(Math.floor(time / 1000 % 60));
      const milliseconds = padMiliseconds(time % 1000);
      const maybeHours = hours > 0 ? `${hours}h :` : '';
      const maybeMinutes = minutes > 0 ? `${minutes}min :` : '';
     
  
      const timeDisplay = `${hours}h:  ${minutes}min : ${seconds}s  .${milliseconds}mils`;
      return timeDisplay;
    }

      function iziMinedDisplay(coins) {
      const rand = Math.random();
  
      const timeDisplay = `${rand}`;
      return timeDisplay;
    }
  
   function padTime(time) {
        if(time < 10) {
          return `0${time}`;
        } else {
          return time;
        }
   }
   function padMiliseconds(time) {
        if(time < 10) {
          return `00${time}`;
        } else if(time < 100) {
          return `0${time}`;
        } else {
          return time;
        }
   }

   export default Timer;