import React from "react";
import { mediaQuery } from "../config/mediaquery";
import { Link } from "react-router-dom";
import { api_production, client } from "../config/global";

class NewPassword extends React.Component {

    constructor() {
        super();
        this.state = {
            loading: false,
            password: "",
            password_confirm: "",
            valid: {
                password: true,
                password_confirm: true
            },
        }
        mediaQuery(this, classes, mediaQueries)
        this.updatePassword = this.updatePassword.bind(this)
    }

    componentWillMount() {

        const url_string = window.location.href
        const url = new URL(url_string);
        const code = url.searchParams.get("validation");
        const email = url.searchParams.get("email");
        const token = window.localStorage.getItem("token");
        
        if (token) {
            window.location = client + "/dashboard"
        } else if (!code || !email) {
            window.location = client + "/"
        }
    }

    validate(password, password_confirm) {
        const input_password = document.querySelector('#password')
        const input_password_confirm = document.querySelector('#password_confirm')
        const class_error = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid red; color: #f1f1f1; padding: 10px "
        const class_success = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid #ffb000; color: #f1f1f1; padding: 10px "

        if (!password) {
            this.setState({ valid: { ...this.state.valid, password: false } })
            input_password.style = class_error
            return false
        } else if (!password_confirm) {
            this.setState({ valid: { password: true, password_confirm: false } })
            input_password.style = class_success
            input_password_confirm.style = class_error
            return false
        } else {
            this.setState({ valid: { password: true, password_confirm: true } })
            input_password.style = class_success
            input_password_confirm.style = class_success
            return true
        }
    }

    updatePassword(e) {
        const url_string = window.location.href
        const url = new URL(url_string);
        const code = url.searchParams.get("validation");
        const email = url.searchParams.get("email");

        const { password, password_confirm } = this.state;

        e.preventDefault();

        if (this.validate(password, password_confirm)) {
            this.setState({ loading: true })

            fetch(`${api_production}/wallet/reset`, {
                method: "POST",
                body: JSON.stringify({ code, email, password }),
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then(res => res.json())
                .then(res => {
                    if (res.passwordUpdated) {
                        Swal.fire({ type: "success", title: "Senha alterada com sucesso.", text: "Enviaremos você de volta a tela de acesso para que possa entrar no nosso sistema." }).then(() => {
                            window.location = client + "/"
                            this.$router.push("/");
                        });
                    } else if (res.walletNotFound) {
                        Swal.fire({ type: "error", title: "Error", text: "Erro de autenticação, solicite novamente alteração de senha." })
                    } else if (res.validationCodeError || res.encryptionError) {
                        Swal.fire({ type: "error", title: "Error", text: "Não foi possível atualizar sua senha, tente enviar novamente uma solicitação." })
                    }
                    this.setState({ loading: false })
                })
        }
    }

    render() {
        const { classes } = this.state

        return (
            <div>
                <main style={classes.CentralizedBlock}>
                    <header style={classes.HeaderBlock}>
                        <img src={require("../img/icon-wallet.png")}></img>
                        <h1 style={classes.HeaderH1}>iZiWallet</h1>
                        <h2 style={classes.HeaderH2}>Recupere a sua senha</h2>
                    </header>
                    <form style={classes.FormLogin} id="login">
                        <ul style={classes.MainFormUl}>
                            <li style={classes.MainFormUlLi}>
                                <label htmlFor="password" style={classes.FormLabel}>
                                    <span>Nova senha</span>
                                    <input id="password"
                                        name="password"
                                        type="password"
                                        placeholder="**********"
                                        style={classes.FormInput}
                                        value={this.state.password}
                                        onChange={(e) => this.setState({ password: e.target.value })} />
                                    <span className={classes.iconInformation}>
                                        {
                                            !this.state.valid.password &&
                                            <i className="fal fa-info-circle" style={classes.FormIcon}></i>
                                        }
                                    </span>
                                </label>
                            </li>
                            <li style={classes.MainFormUlLi}>
                                <label htmlFor="password_confirm" style={classes.FormLabel}>
                                    <span>Confirmar nova senha</span>
                                    <input id="password_confirm"
                                        name="password_confirm"
                                        type="password"
                                        placeholder="**********"
                                        style={classes.FormInput}
                                        value={this.state.password_confirm}
                                        onChange={(e) => this.setState({ password_confirm: e.target.value })} />
                                    <span className={classes.iconInformation}>
                                        {
                                            !this.state.valid.password &&
                                            <i className="fal fa-info-circle" style={classes.FormIcon}></i>
                                        }
                                    </span>
                                </label>
                            </li>
                        </ul>
                        <div style={classes.FormFooter}>
                            <button type="submit" onClick={this.updatePassword} style={classes.FormButton}>
                                {
                                    !this.state.loading ?
                                        <p>Confirmar</p>
                                        :
                                        <div class="spinner">
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                        </div>
                                }
                            </button>
                        </div>
                    </form>
                    <footer style={classes.MainFooter}>
                    </footer>
                </main>
            </div>
        )
    }
}


const mediaQueries = {
    CentralizedBlock: {
        l320_768: {

        },
    }
}

const classes = {
    CentralizedBlock: {
        position: "fixed",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
        width: "300px",
        border: "1px solid #ffb000",
        borderRadius: "5px",
        boxShadow: "0 0 5px #ffb00099"
    },

    HeaderBlock: {
        textAlign: "center",
        color: "#f1f1f1",
        padding: "20px"
    },

    HeaderH1: {
        fontSize: "16px"
    },

    HeaderH2: {
        fontSize: "13px",
        marginBottom: 0
    },

    FormLogin: {
        padding: "0 20px",
        margin: 0
    },

    MainFormUl: {
        listStyle: "none",
        padding: 0,
        margin: 0
    },

    MainFormUlLi: {
        margin: "15px 0"
    },

    FormLabel: {
        position: "relative",
        width: "100%",
        fontSize: "11px",
        color: "#f1f1f1"
    },

    FormInput: {
        boxSizing: "border-box",
        width: "100%",
        background: "transparent",
        border: "none",
        borderBottom: "1px solid #ffb000",
        color: "#f1f1f1",
        padding: "10px"
    },

    FormIcon: {
        position: "absolute",
        bottom: "-1px",
        right: "10px",
        color: "#b82423"
    },

    FormFooter: {
        textAlign: "center"
    },

    FormButton: {
        transition: ".2s linear",
        opacity: ".8",
        padding: "0 30px",
        marginTop: "5px",
        backgroundColor: "#ffb000",
        color: "#f1f1f1",
        borderRadius: "20px",
        fontWeight: "700",
        border: "none",
        cursor: "pointer"
    },

    LdsRing: {
        display: "inline-block",
        position: "relative",
        width: "5px",
        height: "5px"
    },

    ButtonLoader: {
        boxSizing: "border-box",
        display: "block",
        position: "absolute",
        width: "15px",
        height: "15px",
        marginLeft: "10px",
        marginTop: "-7px",
        border: "2px solid #fff",
        borderRadius: "50%",
        animation: "lds-ring-data-v-6de63560 1.2s cubic-bezier(.5,0,.5,1) infinite",
        borderColor: "#fff transparent transparent"
    },

    MainFooter: {
        textAlign: "center",
        padding: "20px",
        color: "#f1f1f1",
        fontWeight: "700",
        fontSize: "10px"
    }
}

export default NewPassword