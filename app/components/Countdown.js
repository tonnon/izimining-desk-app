import React, { Component } from 'react';


export class Countdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      workTime: 3600,
    };

    this.renderTime = this.renderTime.bind(this);
    this.runCountdown = this.runCountdown.bind(this);
    this.handleWorkTime = this.handleWorkTime.bind(this);

    this.isRun = false;
    this.panelTime = 3600;
  };

  runCountdown() {
    this.isRun = this.isRun ? false : true;

    if (this.isRun) {
      this.timer = setInterval((() => {
        let timer = --this.state.workTime;

        if (timer == 0) {
          clearInterval(this.timer);
          this.isRun = false;
          timer = this.panelTime;
        }

        this.setState({
          workTime: timer });

      }).bind(this), 1000);
    } else {
      clearInterval(this.timer);
    }

    return this.state.workTime;
  }

  renderTime(toggleTime) {
    if (toggleTime) {
      return this.runCountdown();
    }

    return this.formatTime(this.state.workTime);
  }

  formatTime(time) {
    let mins = time / 60;
    let sec = time % 60;
    sec = sec < 10 ? '0' + parseInt(sec) : parseInt(sec);

    return parseInt(mins) + 'm' + ':' + sec + 's';
  }

  handleWorkTime(time) {
    time *= 60;
    this.setState({
      workTime: time });

    this.panelTime = time;
  }

  render() {
    return (
      <div>
        <div className="clock">
            <Timer
            renderFunc={this.renderTime} workTime={this.state.workTime} restTime={this.state.restTime}
            />
        </div>
      </div>
      // React.createElement("div", { className: "clock" },
      // React.createElement(Timer, { renderFunc: this.renderTime, workTime: this.state.workTime, restTime: this.state.restTime }))
      // );
    )};
};

class Timer extends React.Component {
  render() {
    return (
      React.createElement("div", { onClick: (() => {this.props.renderFunc(true);}).bind(this), className: "timer-wrap" }, this.props.renderFunc())
      );
  }}

export default Countdown;
