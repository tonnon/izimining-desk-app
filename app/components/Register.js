import React from "react";
import { mediaQuery } from "../config/mediaquery";
import { Link } from "react-router-dom";
import { api_production, client } from "../config/global";
import language from "../config/language";
import Swal from 'sweetalert2';
import routes from "../constants/routes";

class Register extends React.Component {

    constructor() {
        super();
        this.state = {
            loading: false,
            name: "",
            email: "",
            password: "",
            password_confirm: "",
            valid: {
                name: true,
                email: true,
                password: true,
                password_confirm: true
            },
        }
        mediaQuery(this, classes, mediaQueries)
        this.register = this.register.bind(this)
    }

    componentDidMount() {
        const token = window.localStorage.getItem("token");
        if (token) {
            //window.location = `file://${__dirname}/app.html#${routes.DASHBOARD}`
        }
    }

    validate(name, email, password, password_confirm) {
        const input_name = document.querySelector('#name')
        const input_email = document.querySelector('#email')
        const input_password = document.querySelector('#password')
        const input_password_confirm = document.querySelector('#password_confirm')
        const class_error = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid red; color: #f1f1f1; padding: 10px "
        const class_success = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid #ffb000; color: #f1f1f1; padding: 10px "

        if (!name) {
            this.setState({ valid: { ...this.state.valid, name: false } })
            input_name.style = class_error
            return false
        } else if (!email) {
            this.setState({ valid: { ...this.state.valid, name: true, email: false } })
            input_name.style = class_success
            input_email.style = class_error
            return false
        } else if (!password) {
            this.setState({ valid: { ...this.state.valid, name: true, email: true, password: false } })
            input_email.style = class_success
            input_password.style = class_error
            return false
        } else if (!password_confirm) {
            this.setState({ valid: { name: true, email: true, password: true, password_confirm: false } })
            input_password.style = class_success
            input_password_confirm.style = class_error
            return false
        } else if (password != password_confirm) {
            input_password.style = class_error
            input_password_confirm.style = class_error
            Swal.fire({ type: "error", title: language.error_register_title, text: language.error_confirm_password })
            return false
        } else {
            input_password.style = class_success
            input_password_confirm.style = class_success
            this.setState({ valid: { name: true, email: true, password: true, password_confirm: true } })
            return true
        }
    }

    register(e) {
        const { name, email, password, password_confirm } = this.state;

        e.preventDefault();

        if (this.validate(name, email, password, password_confirm)) {
            this.setState({loading: true})

            fetch(`${api_production}/wallet/register`, {
                method: "POST",
                body: JSON.stringify({ name, email, password}),
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then(res => res.json())
                .then(res => {
                    const input_email = document.querySelector('#email')
                    const class_error = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid red; color: #f1f1f1; padding: 10px "
                    const class_success = "box-sizing: border-box; width: 100%; background: transparent; border: none; border-bottom: 1px solid #ffb000; color: #f1f1f1; padding: 10px "

                    if (res.emailSended || res.walletCreated) {
                        this.setState({ valid: { ...this.state.valid, email: true } })
                        input_email.style = class_success
                        Swal.fire({ type: "success", title: language.success_register_title, text: language.success_register }).then(() => {
                            window.location = `#${routes.LOGIN}`
                        });
                    }
                    else if (res.emailexist) {
                        this.setState({ valid: { ...this.state.valid, email: false } })
                        input_email.style = class_error
                        Swal.fire({ type: "error", title: language.error_register_title, text: language.email_exist })
                    }
                    else if (res.invalidEmail) {
                        this.setState({ valid: { ...this.state.valid, email: false } })
                        input_email.style = class_error
                        Swal.fire({ type: "error", title: language.error_register_title, text: language.invalid_email })
                    }
                    else if (res.emailNotConfirmed) {
                        this.setState({ valid: { ...this.state.valid, email: false } })
                        input_email.style = class_error
                        Swal.fire({ type: "error", title: language.error_register_title, text: language.email_not_confirmed })
                    }

                    this.setState({loading: false})
                })
        }
    }

    render() {
        const { classes } = this.state

        return (
            <div>
                <main style={classes.CentralizedBlock}>
                    <header style={classes.HeaderBlock}>
                        <img src={require("../img/icon-wallet.png")}></img>
                        <h1 style={classes.HeaderH1}>iZiWallet</h1>
                        <h2 style={classes.HeaderH2}>{language.create_your_account_now}</h2>
                    </header>
                    <form style={classes.FormLogin} id="login">
                        <ul style={classes.MainFormUl}>
                            <li style={classes.MainFormUlLi}>
                                <label htmlFor="name" style={classes.FormLabel}>
                                    <span>{language.input_name}</span>
                                    <input id="name"
                                        placeholder={language.plc_name}
                                        name="name"
                                        type="text"
                                        style={classes.FormInput}
                                        value={this.state.name}
                                        onChange={(e) => this.setState({ name: e.target.value })} />
                                    <span className="icon-Informacao">
                                        {
                                            !this.state.valid.name &&
                                            <i className="fal fa-info-circle" style={classes.FormIcon}></i>
                                        }
                                    </span>
                                </label>
                            </li>
                            <li style={classes.MainFormUlLi}>
                                <label htmlFor="email" style={classes.FormLabel}>
                                    <span>{language.input_email}</span>
                                    <input id="email"
                                        placeholder={language.plc_email}
                                        name="email"
                                        type="text"
                                        style={classes.FormInput}
                                        value={this.state.email}
                                        onChange={(e) => this.setState({ email: e.target.value })} />
                                    <span className="icon-Informacao">
                                        {
                                            !this.state.valid.email &&
                                            <i className="fal fa-info-circle" style={classes.FormIcon}></i>
                                        }
                                    </span>
                                </label>
                            </li>
                            <li style={classes.MainFormUlLi}>
                                <label htmlFor="password" style={classes.FormLabel}>
                                    <span>{language.input_password}</span>
                                    <input id="password"
                                        name="password"
                                        type="password"
                                        placeholder={language.plc_password}
                                        style={classes.FormInput}
                                        value={this.state.password}
                                        onChange={(e) => this.setState({ password: e.target.value })} />
                                    <span className={classes.iconInformation}>
                                        {
                                            !this.state.valid.password &&
                                            <i className="fal fa-info-circle" style={classes.FormIcon}></i>
                                        }
                                    </span>
                                </label>
                            </li>
                            <li style={classes.MainFormUlLi}>
                                <label htmlFor="password-confirmation" style={classes.FormLabel}>
                                    <span>{language.input_password_confirm}</span>
                                    <input id="password_confirm"
                                        name="password_confirm"
                                        type="password"
                                        placeholder={language.plc_password}
                                        style={classes.FormInput}
                                        value={this.state.password_confirm}
                                        onChange={(e) => this.setState({ password_confirm: e.target.value })} />
                                    <span className={classes.iconInformation}>
                                        {
                                            !this.state.valid.password_confirm &&
                                            <i className="fal fa-info-circle" style={classes.FormIcon}></i>
                                        }
                                    </span>
                                </label>
                            </li>
                        </ul>
                        <div style={classes.FormFooter}>
                            <button type="submit" onClick={this.register} style={classes.FormButton}>
                                {
                                    !this.state.loading ?
                                        <p>{language.btn_register}</p>
                                        :
                                        <div className="spinner">
                                            <div className="bounce1"></div>
                                            <div className="bounce2"></div>
                                            <div className="bounce3"></div>
                                        </div>
                                }
                            </button>
                        </div>
                    </form>
                    <footer style={classes.MainFooter}>
                        <div style={{ display: "flex", flexDirection: "row", justifyContent: "space-around" }}>
                            <p>
                                {language.already_have_account}
                            <br></br>
                                <Link to="/" style={{ textDecorationLine: "none", color: "#ffb000", textAlign: "center" }}>{language.btn_login}</Link>
                            </p>
                        </div>
                    </footer>
                </main>
            </div>
        )
    }
}


const mediaQueries = {
    CentralizedBlock: {
        l320_768: {

        },
    }
}

const classes = {
    CentralizedBlock: {
        position: "fixed",
        top: "50%",
        left: "50%",
        transform: "translate(-50%,-50%)",
        width: "300px",
        border: "1px solid #ffb000",
        borderRadius: "5px",
        boxShadow: "0 0 5px #ffb00099"
    },

    HeaderBlock: {
        textAlign: "center",
        color: "#f1f1f1",
        padding: "20px"
    },

    HeaderH1: {
        fontSize: "16px"
    },

    HeaderH2: {
        fontSize: "13px",
        marginBottom: 0
    },

    FormLogin: {
        padding: "0 20px",
        margin: 0
    },

    MainFormUl: {
        listStyle: "none",
        padding: 0,
        margin: 0
    },

    MainFormUlLi: {
        margin: "15px 0"
    },

    FormLabel: {
        position: "relative",
        width: "100%",
        fontSize: "11px",
        color: "#f1f1f1"
    },

    FormInput: {
        boxSizing: "border-box",
        width: "100%",
        background: "transparent",
        border: "none",
        borderBottom: "1px solid #ffb000",
        color: "#f1f1f1",
        padding: "10px"
    },

    FormIcon: {
        position: "absolute",
        bottom: "-1px",
        right: "10px",
        color: "#b82423"
    },

    FormFooter: {
        textAlign: "center"
    },

    FormButton: {
        transition: ".2s linear",
        opacity: ".8",
        padding: "0 30px",
        marginTop: "5px",
        backgroundColor: "#ffb000",
        color: "#f1f1f1",
        borderRadius: "5px",
        fontWeight: "700",
        border: "none",
        cursor: "pointer"
    },

    LdsRing: {
        display: "inline-block",
        position: "relative",
        width: "5px",
        height: "5px"
    },

    ButtonLoader: {
        boxSizing: "border-box",
        display: "block",
        position: "absolute",
        width: "15px",
        height: "15px",
        marginLeft: "10px",
        marginTop: "-7px",
        border: "2px solid #fff",
        borderRadius: "50%",
        animation: "lds-ring-data-v-6de63560 1.2s cubic-bezier(.5,0,.5,1) infinite",
        borderColor: "#fff transparent transparent"
    },

    MainFooter: {
        textAlign: "center",
        padding: "20px",
        color: "#f1f1f1",
        fontWeight: "700",
        fontSize: "10px"
    }
}

export default Register