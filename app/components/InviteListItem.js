import React from "react";
import { mediaQuery } from "../config/mediaquery";
import { ethereum_url } from "../config/global";
//import { Transform } from "stream";
import language from "../config/language"

class InviteListItem extends React.Component{

    constructor(){
        super();
    
        this.state = {
          InsideContentVisible: false,
          Arrow: true
        };

        mediaQuery(this, classes, mediaQueries)

        this.copyAddress = this.copyAddress.bind(this)
        this.pay = this.pay.bind(this)
    }

    copyAddress(value)
    {
        navigator.clipboard.writeText(value)
        this.props.msgStatus(language.tooltip_address_copied)
    }

    pay()
    {
        this.props.onPay(this.props.data.address)
    }

    operation()
    {
       this.setState({
        ArrowTableTransformHead2: !this.state.ArrowTableTransformHead2
       })
    }


    render(){
        
        const { classes } = this.state
        const { address, date, status, invited } = this.props.data

        return(
            <div>
                <div 
                    onMouseEnter={()=> this.setState({headTableHover: true})} 
                    onMouseLeave={()=> this.setState({headTableHover: false})}       
                    style={{...classes.ItensTable, backgroundColor: this.state.headTableHover ? "#515151" : "transparent"}}>
                    <div style={classes.HeadTable}>
                        <article style={{...classes.Article, ...classes.Art1}}>
                        <div style={classes.ContentFlex}>
                            <div>
                                <p style={classes.InsideP}>{address.slice(0,30)}...</p>
                            </div>
                            <button onClick={() => this.copyAddress(address)} style={classes.IconCopy}><i className="fal fa-copy" style={classes.Copy}></i></button>
                        </div>
                        </article >
                        {
                            window.innerWidth > 768 &&
                            <article style={classes.Article} onClick={()=> this.operation()}>
                                <h2 style={classes.ItensH2}>{maskDate(date)}</h2>
                            </article>
                        }
                        <article 
                            style={{...classes.Article,justifyContent:"center" }}
                            onClick={this.pay}
                            onMouseEnter={()=> this.setState({hover:true})}
                            onMouseLeave={()=> this.setState({hover:false})}>
                            <h2 style={{...classes.ItensH2, width: "78%"}}>{maskStatus(status, this.state.hover)}</h2>
                        </article>
                    </div>
                </div>
            </div>
        )
    }
}


const mask = (value) => {

    let int= "0", dec= "000000000";
    
    if(value){
        value = String(value).split(".").join("")
        if(value.length > 8){
            int = value.slice(0, value.length - 8)
            dec = value.slice(value.length - 8, value.length)
        }else{
            int = "0"
            dec = new Array(8 - value.length).fill(0).join("") + value
        }
    }

    return int + "." + dec
}

const maskDate = (unformatedDate) => {
    if(unformatedDate){
        let date = new Date(unformatedDate);
        return `${date.getFullYear()}-${("0"+ (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)} ${("0" + date.getHours()).slice(-2)}:${( "0" + date.getMinutes()).slice(-2)}`
    }else{
        return `00-00-0000 00:00`
    }
}

const maskStatus = (status, hover) => {
    switch(status){
        case 0:
            return (
                <div style={{
                    ...classes.btnEnviar, 
                    backgroundColor:hover?"#ffb000":"",
                    border: hover? "" : "1px solid white"
                }}><span>{language.invite_item_send}<i class="fas fa-coins"></i></span></div>
            )
        default:
            return language.invite_item_payed
    }
}

const mediaQueries = {
        l320_768:{
            TransactionDiv: {
                width: "100%"
            },
    
            ItensTable: {
                width: "100%"
            },
    
            Top: {
                display: "block",
                textAlign: "center"
            },
    
            Tilt: {
                width: "100%"
            },
    
            BtnAdd: {
                width: "100%"
            },
    
            Search: {
                width: "100%"
            },
    
            SearchTransaction: {
                width: "100%",
                textAlign: "center"
            },
    
            Table: {
                display: "-webkit-inline-box",
                width: "100%"
            },
    
            TableH1: {
                marginRight: "10px"
            }
        }
    }

const classes = {

        ItensTable: {
            width: "100%",
            justifyContent: "space-between",
            padding: "5px 10px",
            alignItems: "center",
            color: "#758696",
            border: "1px solid rgba(192,192,192, 0.2)",
            transition: "all .4s",
        },
        
        HeadTable: {
            cursor: "pointer",
            display: "flex",
            alignItems:"center", 
            justifyContent:"space-around",
            grid: "auto/30% 15% 18% 17% 20%",
            gridColumnGap: "auto",
            gridRowGap: "auto"
        },

        ItensH2: {
            fontSize: ".75em",
            wordBreak: "break-all",
            color: "#f1f1f1",
            fontWeight: "lighter"
        },

        ValueTransaction: {
            color: "#ffb000",
            fontSize: ".75em",
            wordBreak: "break-all",
            fontWeight: "lighter"
        },

        FlexTransaction: {
            display: "flex"
        },

        Arrow: {
            display: "inline-block",
            verticalAlign: "middle",
            fontSize: "24px",
            color: "#ffb000"
        },
        InsideContent: {
            display: "flex",
            justifyContent: "left",
            cursor: "pointer"
        },

        ContentFlex:{
            flex: "0.30 1 0%",
            display:"flex",
            alignItems:"center"
        },

        InsideH3: {
            color: "#f1f1f1",
            fontSize: ".8em",
        },

        InsideP: {
            margin:0,
            color: "#f1f1f1",
            fontSize: ".8em"
        },

        IconCopy: {
            borderRadius: "5px",
            backgroundColor: "#ffb000",
            border: "none",
            height: "32px",
            width: "36px",
            color: "#f1f1f1",
            cursor: "pointer",
            outline: "none",
            marginLeft: 10,
        },

        Copy: {
            display: "inline-block",
            fontWeight: "400",
            lineHeight: 1,
            color: "white",
            fontSize: "23px"
        },  

        Article: {
            flex:1,
            display:"flex",
            justifyContent:"flex-start"
        },

        Art1:{
            flex:2
        },

        btnEnviar:{
            padding:"10px 5px", 
            display:"flex", 
            alignItems:"center", 
            justifyContent:"space-around", 
            border:"1px solid white", 
            color:"white",
            borderRadius:5, 
            fontSize:15
        }
    }

export default InviteListItem