import React from "react";
import { mediaQuery } from "../config/mediaquery";
import { ethereum_url } from "../config/global";
import language from "../config/language";
//import { Transform } from "stream";

class TableList extends React.Component{

    constructor(){
        super();
    
        this.state = {
          InsideContentVisible: false,
          Arrow: true
        };

        mediaQuery(this, classes, mediaQueries)

        this.copyAddress = this.copyAddress.bind(this)
    }

    componentDidMount(){
        this.mediaQuery()
    }

    operation()
    {
       this.setState({
           InsideContentVisible: !this.state.InsideContentVisible,
           ArrowTransform: !this.state.ArrowTransform
       })
    }

    copyAddress(value)
    {
        navigator.clipboard.writeText(value)
        this.props.msgStatus(language.tooltip_address_copied)
    }

    goToEtherScan(hash){
        window.open(ethereum_url + "tx/" + hash, "_blank")
    }

    render(){
        
        const { classes } = this.state
        const { address, value, date, transaction, status, remark, transactionHash} = this.props.data

        return(
                <div 
                    onMouseEnter={()=> this.setState({headTableHover: true})} 
                    onMouseLeave={()=> this.setState({headTableHover: false})}       
                    style={{...classes.ItensTable, backgroundColor: this.state.headTableHover ? "#515151" : "transparent"}}>
                    <div style={classes.HeadTable}>
                        <article>
                        <div style={classes.ContentFlex}>
                            <div>
                                <p style={classes.InsideP}>{address.slice(0,20)}...</p>
                            </div>
                            <button onClick={() => this.copyAddress(address)} style={classes.IconCopy}><img src={require("../img/copy-regular.svg")} style={classes.Copy}></img></button>
                        </div>
                        </article>
                        <article onClick={()=> this.operation()}>
                            <h2 style={classes.ValueTransaction}>{mask(value)}</h2>
                        </article>
                        <article onClick={()=> this.operation()}>
                            <h2 style={classes.ItensH2}>{maskDate(date)}</h2>
                        </article>
                        <article onClick={()=> this.operation()}>
                            <h2 style={classes.ItensH2}>{maskDirection(transaction)}</h2>
                        </article>
                        <article 
                            onClick={()=> this.operation()}
                            style={classes.FlexTransaction}>
                            <h2 style={{...classes.ItensH2, width: "78%"}}>{maskStatus(status)}</h2>
                            <span style={{ ...classes.Arrow, transform: this.state.ArrowTransform ? "rotate(-180deg)" : ""}}><img src={require("../img/sort-down-solid.svg")}></img></span>
                        </article>
                    </div>
                    {
                    this.state.InsideContentVisible ?
                        <div 
                        onClick={()=> this.operation()}
                        style={{...classes.InsideContent}}>
                            <div>
                                <h3 style={classes.InsideH3}>{language.table_comment}</h3>
                                <p style={classes.InsideH3}>{remark}</p>
                            </div>
                            {/* <div style={{marginLeft: window.innerWidth < 768? 0 : 30}}>
                                <h3 style={classes.InsideH3}>Contrato</h3>
                                <p onClick={()=> this.goToEtherScan(transactionHash)} style={{...classes.InsideH3, color: "#ffb000", cursor:"pointer"}}>{transactionHash}</p>
                            </div> */}
                        </div>
                        :null
                    }
                </div>
        )
    }
}


const mask = (value) => {

    let int= "0", dec= "000000000";
    
    if(value){
        value = String(value).split(".").join("")
        if(value.length > 8){
            int = value.slice(0, value.length - 8)
            dec = value.slice(value.length - 8, value.length)
        }else{
            int = "0"
            dec = new Array(8 - value.length).fill(0).join("") + value
        }
    }

    return int + "." + dec
}

const maskDate = (unformatedDate) => {
    if(unformatedDate){
        let date = new Date(unformatedDate);
        return `${date.getFullYear()}-${("0"+ (date.getMonth() + 1)).slice(-2)}-${("0" + date.getDate()).slice(-2)} ${("0" + date.getHours()).slice(-2)}:${( "0" + date.getMinutes()).slice(-2)}`
    }else{
        return `00-00-0000 00:00`
    }
}

const maskStatus = (status) => {
    switch(status){
        case 0:
            return language.table_status_0
        case 1:
            return language.table_status_1
        case 2:
            return language.table_status_2
        case 3:
            return language.table_status_3
        default:
            return language.table_status_default
    }
}

const maskDirection = (direction) => {
    switch(direction){
        case 0:
            return language.table_directon_0
        case 1:
            return language.table_directon_1
        default:
            return language.table_status_default
    }
}

const mediaQueries = {
        l320_768:{
            TransactionDiv: {
                width: "100%"
            },

            ItensTable: {
                width: "94%"
            },

            HeadTable: {
                display: "inline-table"
            },

            InsideContent: {
                display: "block"
            }
        }
    }

const classes = {

        ItensTable: {
            width: "97%",
            justifyContent: "space-between",
            padding: "5px 10px",
            alignItems: "center",
            color: "#758696",
            border: "1px solid rgba(192,192,192, 0.2)",
            transition: "all .4s",
            marginLeft: "auto",
            marginRight: "auto"
        },
        
        HeadTable: {
            cursor: "pointer",
            display: "grid",
            grid: "auto/30% 15% 18% 17% 20%",
            gridColumnGap: "auto",
            gridRowGap: "auto"
        },

        ItensH2: {
            fontSize: ".75em",
            wordBreak: "break-all",
            color: "#f1f1f1",
            fontWeight: "lighter"
        },

        ValueTransaction: {
            color: "#ffb000",
            fontSize: ".75em",
            wordBreak: "break-all",
            fontWeight: "lighter"
        },

        FlexTransaction: {
            display: "flex"
        },

        Arrow: {
            display: "inline-block",
            verticalAlign: "middle",
            width: "15px"
        },
        InsideContent: {
            display: "flex",
            justifyContent: "left",
            cursor: "pointer"
        },

        ContentFlex:{
            flex: "0.30 1 0%",
            display:"flex",
            alignItems:"center"
        },

        InsideH3: {
            color: "#f1f1f1",
            fontSize: ".8em",
            wordBreak: "break-all",
        },

        InsideP: {
            margin:0,
            color: "#f1f1f1",
            fontSize: ".8em"
        },

        IconCopy: {
            borderRadius: "5px",
            backgroundColor: "#ffb000",
            border: "none",
            height: "26px",
            width: "28px",
            color: "#f1f1f1",
            cursor: "pointer",
            outline: "none",
            marginLeft: 10,
            marginTop: 5,
            alignItems: "center",
            },

        Copy: {
            display: "inline-block",
            fontWeight: "400",
            fontSize: "23px",
            width: "15px",
            
        },  
    }

export default TableList