export const pt = {
    inish: 'Terminar',
    access_your_account_now: 'Acesse sua conta agora!',
    create_your_account_now: 'Crie a sua conta agora!',
    creating_wallet: 'Criando carteira!',

    input_name: 'Nome',
    plc_name: 'Digite seu nome completo',
    input_addressee: 'Destinatário',
    input_address: 'Endereço',
    plc_transaction_type_the: 'Digite o',
    plc_transaction_address: 'endereço',
    plc_transaction_email: 'email',
    plc_transaction_who_receive: 'de quem irá receber',
    input_email: 'Email',
    plc_email: 'Digite o Email completo',
    input_value: 'Valor',
    input_remark: 'Comentário',
    plc_remark: 'Escreva um comentário...',
    input_password: 'Senha',
    input_password_confirm: 'Confirmar senha',
    plc_password: '**********',

    btn_register: 'Cadastrar',
    btn_enter: 'Entrar',
    btn_create_account: 'Criar uma conta',
    btn_login: 'Login',

    tooltip_address_copied: 'Endereço copiado!',

    menu_welcome: 'Seja Bem-Vindo',
    menu_mining: 'Mineraçāo',
    menu_invite: 'Convidar',
    menu_exit: 'Sair',
    modal_exit: 'Tem certeza que deseja sair do nosso sistema?',
    btn_exit_yes: 'Sim',
    btn_exit_no: 'Não',

    invite_code_invite: 'Código do Convite',
    invite_request_bonus: 'Solicitar Bonus',
    invite_header_guest: 'Convidado',
    invite_header_date: 'Data',
    invite_header_status: 'Status',
    invite_none: 'Você não tem convidados pendentes.',
    invite_item_send: 'Enviar',
    invite_item_payed: 'Pago',
    invite_pay_error_title: 'Não foi possível enviar.',
    invite_try_again_title: 'Tente enviar novamente, ou entre em contato conosco.',
    invite_address_mail_invalid: 'Endereço de email inválido.',
    invite_invalid_destination: 'Destino inválido.',
    invite_equal_sender_receiver: 'Carteira do emissor repetida na transferência.',
    invite_already_was_sended: 'A transferência já foi enviada.',
    invite_invalid_account: 'Conta inválida.',
    invite_insuficient_balance: 'Balanço insuficiente.',
    invite_success_title: 'Enviado com sucesso.',
    invite_success: 'Essa pessoa ficará feliz em receber seu presente.',
    invite_bonus_request_title: 'Bonus adquirido.',
    invite_bonus_request: 'Parabéns! você merece bonus pelo seu esforço.',
    invite_bonus_error: 'Não foi possível resgatar seu bonus.',
    invite_bonus_error_title: 'Tente resgatar novamente, ou entre em contato conosco.',

    table_comment: 'Comentário',
    table_status_0: 'Pendente',
    table_status_1: 'Sucesso',
    table_status_2: 'Falhou',
    table_status_3: 'Minerando',
    table_status_default: 'Indefinido',
    table_directon_0: 'Enviado',
    table_directon_1: 'Recebido',
    table_add_your: 'Adicione a sua ',
    table_transfer: 'Transferência',
    table_transaction: 'transação',
    table_transaction_ps: 'Obs: Transações podem demorar até um dia útil para serem consolidadas.',
    table_success_title: 'Sucesso',
    table_error_title: 'Erro',
    table_error: 'Erro ao enviar.',
    table_error_address_1: 'Preencha o ',
    table_error_address_2: 'para quem irá receber.',
    table_error_value: 'Preencha o Valor que deseja enviar.',
    table_error_password: 'Digite sua senha para confirmar.',
    table_error_mongo: 'Erro com o nosso banco.',
    table_error_address_mail_invalid: 'Seu endereço está inválido, por favor saia de sua conta, efetue o login e tente novamente.',
    table_error_equal_sender_receiver: 'Endereço repetido, digite o endereço de quem irá receber.',
    table_error_insuficient_balance: 'Saldo insuficiente para fazer esta transferência.',
    table_error_invalid_account: 'Conta inválida, cerifique-se de ter colocado a senha correta.',
    table_error_invalid_destination: 'Endereço inválido, verifique se digitou corretamente.',
    table_success: 'Seu pedido foi enviado, aguarde a pendência da transação.',


    transactions_title: 'Transações',
    btn_new_transactions: 'Nova',
    new_transactions_address: 'Endereço',
    new_transactions_value: 'Valor',
    new_transactions_date: 'Data',
    new_transactions_transaction: 'Transação',
    new_transactions_status: 'Status',
    table_transactions_none: 'Não há dados disponíveis',
    btn_transactions_transfer: 'Transferir',
    btn_transactions_cancel: 'Cancelar',

    balance_current_balance: 'Saldo atual em',
    balance_to_received: 'A receber em',
    balance_address: 'Endereço da Wallet',

    already_have_account: 'Já possui uma conta?',
    wallet_not_found_title: 'Não foi possível encontrar sua conta',
    wallet_not_found: 'Verifique se seu email ou senha foram escritos corretamente!',
    not_authorized_title: 'Verifique seu email!',
    not_authorized: 'Ainda não validamos sua carteira, por favor confirme clicando no link enviado para o seu email.\n Caso ainda não tenha recebido, clique abaixo para reenviarmos. Verifique também o seu lixo eletrônico.',
    btn_not_authorized: 'Reenviar email de confirmação',
    old_user_title: 'Bem-vindo de volta!',
    old_user: 'Verificamos que você é nosso cliente,\nprecisamos fazer uma atualização no seu cadastro para continuar\nte atendendo da melhor forma',
    success_register_title: 'Verifique seu email',
    success_register: 'Verifique o link que enviamos por email para confirmação da sua wallet.',
    error_register_title: 'Error',
    error_confirm_password: "Confirme sua senha corretamente.",
    email_exist: 'este email já foi cadastrado',
    invalid_email: 'este email é inválido',
    email_not_confirmed: 'este email não foi confirmado',
    wallet_already_exists_title: 'Carteira já está registrada.',
    wallet_already_exists: 'Caso esteja enfrentando problemas para entrar na sua conta entre em contato conosco.',
    wallet_created_error_title: 'Não foi possível criar sua carteira.',
    wallet_created_error: 'Tente confirmar novamente, ou entre em contato conosco.',
    wallet_created_title: 'Carteira criado com sucesso.',
    wallet_created: 'Enviaremos você de volta a tela de acesso para que possa entrar no nosso sistema.',
}

export const en = {
    finish: 'Finish',
    access_your_account_now: 'Sign in to your account now!',
    create_your_account_now: 'Create your account now!',
    creating_wallet: 'Creating wallet!',

    input_name: 'Name',
    plc_name: 'Enter your full name',
    input_addressee: 'Recipient',
    input_address: 'Address',
    plc_transaction_type_the: 'Enter the',
    plc_transaction_address: 'address',
    plc_transaction_email: 'email',
    plc_transaction_who_receive: 'from whom you will receive',
    input_email: 'Email',
    plc_email: 'Enter full email',
    input_value: 'Value',
    input_remark: 'Comment',
    plc_remark: 'Write a comment ...',
    input_password: 'Password',
    input_password_confirm: 'Confirm password',
    plc_password: '**********',

    btn_register: 'Register',
    btn_enter: 'Enter',
    btn_create_account: 'Create an account',
    btn_login: 'Login',

    tooltip_address_copied: 'Address copied!',

    menu_welcome: 'Welcome',
    menu_mining: 'Minería',
    menu_invite: 'Invite',
    menu_exit: 'Exit',
    modal_exit: 'Are you sure you want to exit our system?',
    btn_exit_yes: 'Yes',
    btn_exit_no: 'No',

    invite_code_invite: 'Invite Code',
    invite_request_bonus: 'Request Bonus',
    invite_header_guest: 'Guest',
    invite_header_date: 'Data',
    invite_header_status: 'Status',
    invite_none: 'You have no pending guests.',
    invite_item_send: 'Send',
    invite_item_payed: 'Payment',
    invite_pay_error_title: 'Could not send.',
    invite_try_again_title: 'Please try submitting again, or contact us.',
    invite_address_mail_invalid: 'Invalid email address.',
    invite_invalid_destination: 'Invalid destination.',
    invite_equal_sender_receiver: "Sender's wallet repeated on transfer.",
    invite_already_was_sended: 'A transfer has already been sent.',
    invite_invalid_account: 'Invalid account.',
    invite_insuficient_balance: 'Insufficient balance.',
    invite_success_title: 'Successfully Sent.',
    invite_success: 'This person will be happy to receive your gift.',
    invite_bonus_request_title: 'Bonus purchased.',
    invite_bonus_request: 'Congratulations! you deserve bonuses for your effort. ',
    invite_bonus_error: 'Your bonus could not be redeemed.',
    invite_bonus_error_title: 'Please try to redeem, or contact us.',

    table_comment: 'Comment',
    table_status_0: 'Pending',
    table_status_1: 'Success',
    table_status_2: 'Failed',
    table_status_3: 'Mining',
    table_status_default: 'Undefined',
    table_directon_0: 'Sent',
    table_directon_1: 'Received',
    table_add_your: 'Add your own ',
    table_transfer: 'Transfer',
    table_transaction: 'transaction',
    table_transaction_ps: 'Note: Transactions can take up to 1 business day to be consolidated.',
    table_success_title: 'Success',
    table_error_title: 'Error',
    table_error: 'Error sending.',
    table_error_address_1: 'Fill in',
    table_error_address_2: 'to whom you will receive.',
    table_error_value: 'Fill in the value you want to send.',
    table_error_password: 'Enter your password to confirm.',
    table_error_mongo: 'Error with our bank.',
    table_error_address_mail_invalid: 'Your address is invalid, please log out of your account and try again.',
    table_error_equal_sender_receiver: 'Repeat address, enter the address of the recipient.',
    table_error_insuficient_balance: 'Not enough balance to make this transfer.',
    table_error_invalid_account: 'Invalid account, make sure you have entered the correct password.',
    table_error_invalid_destination: 'Invalid address, make sure you typed correctly.',
    table_success: 'Your order has been sent, please wait for the transaction to be pending.',

    transactions_title: 'Transactions',
    btn_new_transactions: 'New',
    new_transactions_address: 'Address',
    new_transactions_value: 'Value',
    new_transactions_date: 'Data',
    new_transactions_transaction: 'Transaction',
    new_transactions_status: 'Status',
    table_transactions_none: 'No data available',
    btn_transactions_transfer: 'Transfer',
    btn_transactions_cancel: 'Cancel',

    balance_current_balance: 'Current balance in',
    balance_to_received: 'Receiving on',
    balance_address: 'Wallet Address',

    already_have_account: 'Already have an account?',
    wallet_not_found_title: 'Your account could not be found',
    wallet_not_found: 'Make sure your email or password has been spelled correctly!',
    not_authorized_title: 'Check your email!',
    not_authorized: 'We have not yet validated your wallet, please confirm by clicking on the link sent to your email.\nIf you have not yet received, click below to resubmit. Also check your junk. ',
    btn_not_authorized: 'Resend confirmation email',
    old_user_title: 'Welcome back!',
    old_user: 'We have verified that you are our client,\nwe need to update your registry to continue\nteplying the best way',
    success_register_title: 'Check your email',
    success_register: 'Check the link we send via email to confirm your wallet.',
    error_register_title: 'Error',
    error_confirm_password: "Confirm your password correctly.",
    email_exist: 'this email has already been registered',
    invalid_email: 'this email is invalid',
    email_not_confirmed: 'this email has not been verified',
    wallet_already_exists_title: 'Wallet is already registered.',
    wallet_already_exists: 'If you are experiencing problems entering your account please contact us.',
    wallet_created_error_title: 'Could not create your wallet.',
    wallet_created_error: 'Please try to confirm again, or contact us.',
    wallet_created_title: 'Wallet created successfully.',
    wallet_created: 'We will send you back the login screen so you can enter our system.',
}

export const es = {
    access_your_account_now: 'Acceda a su cuenta ahora!',
    create_your_account_now: '¡Cree su cuenta ahora!',
    creating_wallet: 'creando una cartera!',

    input_name: 'Nombre',
    plc_name: 'Escriba su nombre completo',
    input_addressee: 'Destinatario',
    input_address: 'Dirección',
    plc_transaction_type_the: 'Escriba el',
    plc_transaction_address: 'dirección',
    plc_transaction_email: 'email',
    plc_transaction_who_receive: 'de quien va a recibir',
    input_email: 'Email',
    plc_email: 'Escriba el email completo',
    input_value: 'Valor',
    input_remark: 'Comentario',
    plc_remark: 'Escribe un comentario ...',
    input_password: 'Contraseña',
    input_password_confirm: 'Confirmar contraseña',
    plc_password: '***********',

    btn_register: 'Registrarse',
    btn_enter: 'Entrar',
    btn_create_account: 'Crear una cuenta',
    btn_login: 'Login',

    tooltip_address_copied: 'Dirección copiada!',

    menu_welcome: 'Sea Bienvenida',
    menu_mining: 'Minería',
    menu_invite: 'Invitar',
    menu_exit: 'Salir',
    modal_exit: '¿Está seguro de que desea salir de nuestro sistema?',
    btn_exit_yes: 'Sí',
    btn_exit_no: 'No',

    invite_code_invite: 'Código de la invitación',
    invite_request_bonus: 'Solicitar Bonus',
    invite_header_guest: 'Invitado',
    invite_header_date: 'Fecha',
    invite_header_status: 'Status',
    invite_none: 'Usted no tiene invitados pendientes.',
    invite_item_send: 'Enviar',
    invitado: 'Pago',
    invite_pay_error_title: 'No se pudo enviar.',
    invite_try_again_title: 'Intente enviar de nuevo, o entre en contacto con nosotros.',
    invite_address_mail_invalid: 'Dirección de email no válida.',
    invite_invalid_destination: 'Destino no válido.',
    invite_equal_sender_receiver: 'Carpeta del emisor repetida en la transferencia.',
    invite_already_was_sended: 'Ya se ha enviado una transferencia.',
    invite_invalid_account: 'Cuenta no válida.',
    invite_insuficient_balance: 'Balance insuficiente.',
    invite_success_title: 'Enviado con éxito.',
    invite_success: 'Esa persona estará feliz de recibir su regalo.',
    invite_bonus_request_title: 'Bonus adquirido.',
    invite_bonus_request: '¡Enhorabuena! que se merece el bono por su esfuerzo. ',
    invite_bonus_error: 'No se pudo rescatar su bono.',
    invite_bonus_error_title: 'Intente rescatar de nuevo, o entre en contacto con nosotros.',

    table_comment: 'Comentario',
    table_status_0: 'Pendiente',
    table_status_1: 'Éxito',
    table_status_2: 'Fallo',
    table_status_3: 'Minerando',
    table_status_default: 'Indefinido',
    table_directon_0: 'Enviado',
    table_directon_1: 'Recibido',
    table_add_your: 'Agregue la suya',
    table_transfer: 'Transferencia',
    table_transaction: 'transacción',
    table_transaction_ps: 'Nota: Las transacciones pueden tardar hasta un día útil para ser consolidadas.',
    table_success_title: 'Éxito',
    table_error_title: 'Error',
    table_error: 'Error al enviar.',
    table_error_address_1: 'Rellene el',
    table_error_address_2: 'para quien recibirá.',
    table_error_value: 'Rellene el valor que desea enviar.',
    table_error_password: 'Escriba su contraseña para confirmar.',
    table_error_mongo: 'Error con nuestro banco.',
    table_error_address_mail_invalid: 'Su dirección está invalida, por favor salga de su cuenta inicie sesión e inténtelo de nuevo.',
    table_error_equal_sender_receiver: 'Dirección repetida, introduzca la dirección de quién recibirá.',
    table_error_insuficient_balance: 'Saldo insuficiente para hacer es transferencia.',
    table_error_invalid_account: 'Cuenta no válida, asegúrese de haber colocado la contraseña correcta.',
    table_error_invalid_destination: 'Dirección no válida, compruebe que ha escrito correctamente.',
    table_success: 'Su solicitud ha sido enviada, espere a la espera de la transacción.',

    transaction_title: 'Transacciones',
    btn_new_transactions: 'Nueva',
    new_transactions_address: 'Dirección',
    new_transactions_value: 'Valor',
    new_transactions_date: 'Fecha',
    new_transactions_transaction: 'Transacción',
    new_transactions_status: 'Status',
    table_transactions_none: 'No hay datos disponibles',
    btn_transactions_transfer: 'Transferir',
    btn_transactions_cancel: 'Cancelar',

    balance_current_balance: 'Saldo actual en',
    balance_to_received: 'Recibir en',
    balance_address: 'Dirección de Wallet',

    already_have_account: '¿Ya tienes una cuenta?',
    wallet_not_found_title: 'No se pudo encontrar su cuenta',
    wallet_not_found: '¡Compruebe si su email o contraseña se han escrito correctamente!',
    not_authorized_title: '¡Compruebe su email!',
    not_authorized: 'no se ha confirmado su cartera, por favor confirme haciendo clic en el enlace enviado a su email. \ n Si aún no ha recibido, haga clic abajo para reenviar. Compruebe también su email no deseado. ',
    btn_not_authorized: 'Reenviar email de confirmación',
    old_user_title: 'Bienvenido de vuelta!',
    old_user: 'Comprobamos que usted es nuestro cliente, \ nprecisamos hacer una actualización en su registro para continuar \ nte atendiendo de la mejor forma',
    success_register_title: 'Compruebe su email',
    success_register: 'Compruebe el enlace que enviamos por email para la confirmación de su wallet.',
    error_register_title: 'Error',
    error_confirm_password: "Confirme su contraseña correctamente.",
    email_exist: 'este email se ha registrado',
    invalid_email: 'este email no es válido',
    email_not_confirmed: 'este email no ha sido confirmado',
    wallet_already_exists_title: 'Carpeta ya está registrada.',
    wallet_already_exists: 'Si se enfrenta a problemas para entrar en su cuenta, póngase en contacto con nosotros.',
    wallet_created_error_title: 'No se pudo crear su cartera.',
    wallet_created_error: 'Intente confirmar de nuevo, o entre en contacto con nosotros.',
    wallet_created_title: 'Cartera creada con éxito.',
    wallet_created: 'Le enviaremos de nuevo la pantalla de acceso para que pueda entrar en nuestro sistema.',
}

const getLanguage = () => {
    switch ("pt") {
        case "pt":
            return pt
        case "en":
            return en
        case "es":
            return es
        default:
            return en
    }
}

export default getLanguage()