export const mediaQuery = (component, classes, queries) => {
    component.state = {
        ...component.state,
        classes
    }
    
    window.addEventListener("resize",  () => resize(component, classes, queries))
    component.mediaQuery = () => resize(component, classes, queries)
  
}


const resize = (component, outerclasses, queries) => {

    let classes = {...outerclasses}
    let realWidth = window.innerWidth;

    let extra = Object.keys(queries).find(width_label => {
        let widths = {min: Number(width_label.replace(/l|_\d*/g,"")), max: Number(width_label.replace(/l\d*_/g,""))}
        return widths.min <= realWidth && widths.max >= realWidth
    })

    if(extra){
        Object.keys(queries[extra]).forEach(cssAtribute => {
            let modification = queries[extra][cssAtribute]
            if(modification){
                classes[cssAtribute] = {...classes[cssAtribute], ...modification}
            }
        })
    }else{
        classes = outerclasses
    }


    component.setState({classes})
}
