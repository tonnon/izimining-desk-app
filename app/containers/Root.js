// @flow
import React, { Component } from 'react';
import { ConnectRouter, ConnectedRouter } from 'connected-react-router';
import Routes from '../Routes';


export default class Root extends Component{
  render() {
    return (
        <Routes />
    );
  }
}
